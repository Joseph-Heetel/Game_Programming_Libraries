#include <chrono>
#include <iostream>
#include <jht_ecs.hpp>
#include <jht_os.hpp>
#include <jht_os_event.hpp>
#include <thread>

namespace jht::test {

    class EventPrintService : public ecs::IService, public os::event::BaseListener
    {
      public:
        virtual std::u8string_view GetId() const override { return u8"jht::test::EventPrintService"; }

        virtual void OnOsEvent(const os::event::Base* event) override
        {
            auto button = (const os::event::InputButton*)event;
            if(!!button)
            {
                bool state = button->Pressed;
                std::cout << state << std::endl;
            }
        }
    };

    void TestOs()
    {
        ecs::ServiceProvider sp;
        sp.AddSingleton<os::EventManager>();
        sp.AddSingleton<ecs::CallbackHub>();
        sp.AddSingleton<EventPrintService>();
        os::OsInterface* app    = sp.AddSingleton<os::OsInterface>();
        os::Window*      window = app->CreateWindow(os::Window::CreateInfo());
        // for(auto win : app->Windows)
        // {
        //     std::cout << (const char*)win->GetTitle().data() << std::endl;
        // }
        for(int i = 0; i < 10000; i++)
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
            while(app->PollAndDistributeEvent())
                ;
        }
        app->DestroyWindow(window);
    }
}  // namespace jht::test
