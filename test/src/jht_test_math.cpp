#include <chrono>
#include <iostream>
#include <jht_math.hpp>

namespace jht::test {

    void SpeedtestMatrixVectorMult()
    {
        using namespace jht::math;
        using namespace std::chrono;

        // Defer matrix row load:       75202624
        // Preload all 4 matrix rows:   75201921

        // Packed:      75318046 - slightly slower
        // Incremental: 75207292 - slightly faster
        // Note: in

        Mat4 matrix(1.2f, 1.03f, 0.4f, 3.f, 5.f, 1.f, 1.1f, 1.2f, -0.4f, -1.f, 0.8f, -.3f, -2.f, -1.f, -.2f, -.2f);

        Vec4 out;

        steady_clock clock;

        auto start = clock.now();

        for(int32_t i = 0; i < 100000000; i++)
        {
            Vec4 vector(1.05f, 2.01f, -.3f, 1.5f);
            vector = matrix * vector;
            vector = matrix * vector;
            vector = matrix * vector;
            vector = matrix * vector;
            vector = matrix * vector;
            vector = matrix * vector;
            vector = matrix * vector;
            vector = matrix * vector;
            vector = matrix * vector;
            vector = matrix * vector;
            vector = matrix * vector;
            out += vector * (1.f / 500000.f);
        }

        auto end = clock.now();

        auto duration = end - start;


        std::cout << "Matrix vector multiplication test took " << duration.count() << " nanoseconds. " << out[0] << std::endl;
    }

    void SpeedtestMatrixMatrixMult()
    {
        using namespace jht::math;
        using namespace std::chrono;

        // parallel column calculation:     229005356 much faster
        // incremental column calculation:  292253404

        // preload all 4 lefthandside columns:  229097003 no appreciable difference
        // incrementally load columns:          229005356

        // 751747

        Mat4 matrix0(1.2f, 1.03f, 0.4f, 3.f, 1.f, 1.f, 1.1f, 1.2f, -0.4f, -1.f, 0.8f, -.3f, -1.2f, -1.f, -.2f, -.2f);
        Mat4 matrix1(0.8f, 1.04f, 0.8f, 1.f, 2.f, 2.f, 1.7f, 1.1f, -0.8f, -1.f, 0.9f, -1.3f, 1.1f, -.6f, -.8f, -.1f);

        Vec4 out;

        steady_clock clock;

        auto start = clock.now();

        for(int32_t i = 0; i < 1000000; i++)
        {
            Vec4 vector(1.05f, 2.01f, -.3f, 1.5f);
            vector = matrix0 * matrix1 * matrix0 * matrix1 * matrix1 * matrix0 * matrix0 * vector;
            vector = matrix0 * matrix1 * matrix0 * matrix1 * matrix1 * matrix0 * matrix0 * vector;
            vector = matrix0 * matrix1 * matrix0 * matrix1 * matrix1 * matrix0 * matrix0 * vector;
            vector = matrix0 * matrix1 * matrix0 * matrix1 * matrix1 * matrix0 * matrix0 * vector;
            vector = matrix0 * matrix1 * matrix0 * matrix1 * matrix1 * matrix0 * matrix0 * vector;
            vector = matrix0 * matrix1 * matrix0 * matrix1 * matrix1 * matrix0 * matrix0 * vector;
            vector = matrix0 * matrix1 * matrix0 * matrix1 * matrix1 * matrix0 * matrix0 * vector;
            vector = matrix0 * matrix1 * matrix0 * matrix1 * matrix1 * matrix0 * matrix0 * vector;
            vector = matrix0 * matrix1 * matrix0 * matrix1 * matrix1 * matrix0 * matrix0 * vector;
            out += vector * (1.f / 500000.f);
        }

        auto end = clock.now();

        auto duration = end - start;


        std::cout << "Matrix vector multiplication test took " << duration.count() << " nanoseconds. " << out[0] << std::endl;
    }

    void TestMath()
    {
        using namespace jht::math;

        // for(int32_t i = 0; i < 10; i++)
        // {
        //     SpeedtestMatrixMatrixMult();
        // }
        // for(int32_t i = 0; i < 10; i++)
        // {
        //     SpeedtestMatrixVectorMult();
        // }
        {
            Vec2 vec0;
            Vec2 vec1(2.f);
            bool test = vec0 == vec1;

            Vec3 vec2(vec0, 0.f);
            Vec3 vec3(4.f, vec1);

            auto vec4 = vec0 + vec1;
        }

        {
            Mat4 matrix0(1.f);
            Mat4 matrix1(0.8f, 1.04f, 0.8f, 1.f, 2.f, 2.f, 1.7f, 1.1f, -0.8f, -1.f, 0.9f, -1.3f, 1.1f, -.6f, -.8f, -.1f);
            auto row0 = matrix1.LoadM128Row<1>();
            auto row1 = matrix1.LoadM128Row(1);
            Mat4 matrix2(2.f);
            Mat4 multi = matrix0 * matrix1.Transposed();
        }
        {
            Mat4 matrixA(2, 3, 4, 5, -1, -2, -4, -3, 0, 1, 5, 1, 8, 0, -1, 0);
            Mat4 matrixB(6, 5, 4, 0, 1, 2, 7, 5, 6, 5, 4, 5, 7, 5, 8, 6);

            Vec4 test(4, 2, 3, 5);
            Vec4 result0 = (matrixA * matrixB) * test;
            Vec4 result1 = matrixA * (matrixB * test);
            Vec4 diff    = result0 - result1;
            Vec4 norm    = Normalize(result0);
            Vec3 e;
        }
        {
            Mat4 matrixA(1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
            Mat4 matrixB(2, 5, 4, 0, 1, 2, 1, 5, 6, 5, 4, 5, 2, 5, 3, 6);
            Mat4 matrixC = matrixA * matrixB;
            Mat4 matrixD = matrixB * matrixA;
            Vec3 e;
        }
        {
            Mat4 matrixA(2, 5, 4, 0, 1, 2, 1, 5, 6, 5, 4, 5, 2, 5, 3, 6);
            Vec4 vec0(4, 2, 3, 5);
            Vec4 vec1 = matrixA * vec0;
            Vec3 e;
        }
    }
}  // namespace jht::test
