#include <iostream>
#include <jht_ecs.hpp>

namespace jht::test {

    using namespace jht::ecs;

    class MyEvent0 : public IEventListener<float>
    {
      public:
        virtual std::u8string_view GetId() const override { return u8"jht::test::MyEvent0"; };
        virtual void               Callback(TArg arg) override { OnMyEvent0(arg); };

        virtual void OnMyEvent0(TArg arg) = 0;
    };

    class MyCustomEventArgs
    {
      public:
        float         Value0;
        std::u8string Value1;
    };
    class MyEvent1 : public IOrderedEventListener<MyCustomEventArgs&>
    {
      public:
        virtual std::u8string_view GetId() const override { return u8"jht::test::MyEvent1"; };
        virtual void               Callback(TArg arg) override { OnMyEvent1(arg); };

        virtual void OnMyEvent1(TArg arg) = 0;
    };

    class MyCallbackListConfig0 : public ICallbackListConfig
    {
      public:
        virtual std::u8string_view              GetId() const override { return u8"jht::test::MyCallbackListConfig0"; };
        virtual std::vector<ICallbackListBase*> GetCallbackLists() const override { return {new SimpleCallbackList<MyEvent0>()}; };
    };
    class MyCallbackListConfig1 : public ICallbackListConfig
    {
      public:
        virtual std::u8string_view              GetId() const override { return u8"jht::test::MyCallbackListConfig1"; };
        virtual std::vector<ICallbackListBase*> GetCallbackLists() const override { return {new OrderedCallbackList<MyEvent1>()}; };
    };
    class MyComponent0 : public Component, public MyEvent0, public MyEvent1
    {
      public:
        explicit MyComponent0(Registry* registry) : Component(registry), MyEvent0(), MyEvent1() {}
        virtual std::u8string_view GetId() const override { return u8"jht::test::MyComponent0"; };
        virtual void               OnMyEvent0(float arg) override {}
        virtual void               OnMyEvent1(MyCustomEventArgs& arg) override { std::cout << "MyComponent0::MyEvent1: " << GetOrder() << std::endl; }
    };
    class MyComponent1 : public Component, public MyEvent0, public MyEvent1
    {
      public:
        explicit MyComponent1(Registry* registry) : Component(registry), MyEvent0(), MyEvent1() {}
        virtual std::u8string_view GetId() const override { return u8"jht::test::MyComponent1"; };
        virtual uint32_t           GetOrder() const override { return 1001U; }
        virtual void               OnMyEvent0(float arg) override {}
        virtual void               OnMyEvent1(MyCustomEventArgs& arg) override { std::cout << "MyComponent1::MyEvent1: " << GetOrder() << std::endl; }
    };
    class MyService0 : public IService
    {
    };
    class MyService1 : public IService, public ServiceDependent
    {
    };

    void TestEcs()
    {
        std::cout << "Beginning jht::ecs tests ..." << std::endl;
        jht::ecs::ServiceProvider provider;
        provider.AddSingleton<MyCallbackListConfig0>();
        provider.AddSingleton<MyCallbackListConfig1>();
        jht::ecs::CallbackHub* callbackHub = provider.AddSingleton<jht::ecs::CallbackHub>();

        {
            jht::ecs::Registry registry(&provider);
            MyComponent1*      component1 = registry.MakeComponent<MyComponent1>();
            MyComponent0*      component0 = registry.MakeComponent<MyComponent0>();

            MyCustomEventArgs args{.Value0 = 69.f, .Value1 = u8"heh"};
            callbackHub->Invoke<MyEvent1>(args);
        }
        std::cout << "Finished jht::ecs tests" << std::endl;
    }
}  // namespace jht::test