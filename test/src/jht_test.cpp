#include "jht_test.hpp"
#include <iostream>


int main()
{
    try
    {
        jht::test::TestOs();
        jht::test::TestMath();
        jht::test::TestEcs();
        std::cout << "All tests passed" << std::endl << std::flush;
        return 0;
    }
    catch(const char8_t* e)
    {
        std::cerr << "Test failed: " << reinterpret_cast<const char*>(e) << std::endl;
        return -1;
    }
    catch(std::u8string_view e)
    {
        std::cerr << "Test failed: " << reinterpret_cast<const char*>(e.data()) << std::endl;
        return -1;
    }
    catch(const std::exception& e)
    {
        std::cerr << "Test failed: " << e.what() << std::endl;
        return -1;
    }
}