# Types overview

## [`Component` class](./src/jht_ecs_component.hpp)

Base class for registry level components. Components can interact with other components assigned to the same registry and get custom event callbacks. Component class implementations need to have a constructor featuring a single parameter: Registry*

## [`Registry` class](./src/jht_ecs_registry.hpp)

Class providing a non-unique context for combining multiple components. Provides access to the service provider, registers and unregisters callback, and provides functionality to interact with other components.

## [`IService` class](./src/jht_ecs_service.hpp)

Base class for unique singletons (relative to the service providers scope). Service objects can be directly injected, built by the service provider immediately or lazily.

## [`ServiceProvider` class](./src/jht_ecs_serviceprovider.hpp)

Class providing access to all singletons. All methods of this type are recursively thread safe.

## [`ServiceDependent` class](./src/jht_ecs_servicedependent.hpp)

Base class for any type requiring a service provider to function. Constructor asserts that the pointer passed is not null.

## [`CallbackHub` class](./src/jht_ecs_callbackhub.hpp)

Class combining multiple callback lists and allowing registering/unregistering event listeners and invoking events.

## [`CallbackList...` classes](./src/jht_ecs_callbacklistbase.hpp) ([Simple](./src/jht_ecs_simplecallbacklist.hpp) and [Ordered](./src/jht_ecs_orderedcallbacklist.hpp) variants)

Base classes for registering, unregistering and invoking event (listeners)

## [`IEventListener` classes](./src/jht_ecs_event.hpp)

Base classes for defining custom events in a manner that allows invoking events.

# Examples

## Service example

```cpp
#include <jht_ecs.hpp>

namespace mynamespace
{
    // Parameterless constructor example

    class MyService : public jht::ecs::Service
    {
      public:
        inline MyService(){}

        virtual std::u8string_view GetId() const override
        {
            return u8"mynamespace::MyService";
        }
    };

    // ServiceProvider dependent example

    class MyService2 : public jht::ecs::Service, public jht::ecs::ServiceDependent
    {
      public:
        inline explicit MyService(ServiceProvider* serviceProvider) : jht::ecs::Service(), jht::ecs::ServiceDependent(serviceProvider) {}

        virtual std::u8string_view GetId() const 
        {
            return u8"mynamespace::MyService";
        }
    };

    // usage

    void immediate()
    {
        jht::ecs::ServiceProvider serviceProvider;
        serviceProvider.AddSingleton<MyService>(); // The service provider immediately instantiates and adds to service collection
        serviceProvider.AddSingleton<MyService2>(); // The service provider immediately instantiates and adds to service collection. The serviceProvider instance is passed to the constructor
        serviceProvider.AddSingleton(new MyService()); // the service provider replaces previous instance. 
    }

    void lazy()
    {
        jht::ecs::ServiceProvider serviceProvider;
        serviceProvder.AddLazy(u8"mynamespace::MyService", [](ServiceProvider*){return new MyService();}); // Adds the lazy builder to the lazy collection
    }
}
```

## Component

```cpp
#include <jht_ecs.hpp>

namespace mynamespace
{
    class MyComponent : public jht::ecs::Component
    {
      public:
        inline explicit MyComponent(ecs::Registry* registry) : ecs::Component(registry) {}
        virtual std::u8string_view GetId() const override
        {
            return u8"mynamespace::MyComponent";
        }
    };
}
```

## Event

```cpp
#include <jht_ecs.hpp>
#include <iostream>

namespace mynamespace
{
    // Custom Args type
    class UpdateEventArgs
    {
      public:
        float DeltaTime = 0.f;
    };

    /// Custom base class which can be inherited from to implement the event listening function
    class UpdateEvent : public jht::ecs::OrderedEventListener<const UpdateEventArgs&>
    {
      public:
        virtual void OnUpdate(const UpdateEventArgs& args) = 0;
        virtual std::u8string_view GetId() const override 
        {
            return u8"mynamespace::UpdateEvent";
        }
        virtual void Callback(TArg arg) override
        {
            OnUpdate(arg);
        }
    };

    // Custom event config to configure the callback hub with
    class CustomEventConfig : public ICallbackListConfig
    {
      public:
        virtual std::u8string_view GetId() const override
        {
            return u8"mynamespace::CustomEventConfig";
        }
        virtual std::vector<ecs::CallbackListBase*> GetCallbackLists() const override
        {
            return 
            {
                new jht::ecs::OrderedCallbackList<UpdateEvent>(),
            }; 
        }
    };

    // Component implementing the update event
    class MyComponent : public jht::ecs::Component, public UpdateEvent
    {
      public:
        inline explicit MyComponent(ecs::Registry* registry) : ecs::Component(registry) {}

        virtual uint32_t GetOrder() const override
        {
            return 2000U; // Order bigger than default 1000U, so will be invoked later
        }
        virtual void OnUpdate(const UpdateEventArgs& args) override
        {
            // Update handler method
            std::cout << "Update event delta " << args.DeltaTime << std::endl;
        }
        virtual std::u8string_view GetId() const override
        {
            return u8"mynamespace::MyComponent";
        }
    };

    // usage
    void test()
    {
        jht::ecs::ServiceProvider provider;
        provider.AddSingleton<CustomEventConfig>();
        jht::ecs::CallbackHub* callbackHub = provider.AddSingleton<jht::ecs::CallbackHub>(); // Will get event config from service provider. Will have callback list for UpdateEvent right from the start
        
        { // Registry needs to be finalized before service provider. In this case it goes out of scope first.
            jht::ecs::Registry registry(&provider);
            MyComponent* component = registry.MakeComponent<MyComponent>();

            UpdateEventArgs args{.DeltaTime=0.11f};
            callbackHub->Invoke<UpdateEvent>(args); // Invoke the event
        } // Registry finalizes components, then itself
    } // ServiceProvider finalizes services, then itself
}
```