#pragma once
#include "jht_ecs_callbacklistbase.hpp"
#include "jht_ecs_declares.hpp"
#include "jht_ecs_event.hpp"
#include <jht_basics.hpp>
#include <vector>

namespace jht::ecs {
    template <typename TEventListener>
    class SimpleCallbackList : public ICallbackList<TEventListener>
    {
      public:
        inline virtual void Add(IEventReceiver* listener) override;
        inline virtual void Remove(IEventReceiver* listener) override;

        virtual inline void Invoke(typename TEventListener::TArg arg) override;

        inline virtual ~SimpleCallbackList() { _listeners.clear(); }

      protected:
        std::vector<TEventListener*> _listeners;
    };

    template <typename TEventListener>
    inline void SimpleCallbackList<TEventListener>::Add(IEventReceiver* listener)
    {
        auto cast = dynamic_cast<TEventListener*>(listener);

        if(!!cast)
        {
            _listeners.push_back(cast);
        }
    }

    template <typename TEventListener>
    inline void SimpleCallbackList<TEventListener>::Remove(IEventReceiver* listener)
    {
        auto cast = dynamic_cast<TEventListener*>(listener);

        if(!cast)
        {
            return;
        }
        for(auto iter = _listeners.begin(); iter != _listeners.end(); ++iter)
        {
            if(cast == *iter)
            {
                _listeners.erase(iter);
                return;
            }
        }
    }

    template <typename TEventListener>
    inline void SimpleCallbackList<TEventListener>::Invoke(typename TEventListener::TArg arg)
    {
        for(TEventListener* listener : _listeners)
        {
            listener->Callback(arg);
        }
    }
}  // namespace jht::ecs
