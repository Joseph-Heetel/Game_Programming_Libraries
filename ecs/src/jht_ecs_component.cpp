#include "jht_ecs_component.hpp"
#include "jht_ecs_registry.hpp"

namespace jht::ecs {
    Component::Component(Registry* registry) : ServiceDependent(registry->GetServiceProvider()), _registry(registry) {}
}  // namespace jht::ecs
