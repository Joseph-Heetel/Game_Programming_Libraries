#include "jht_ecs_registry.hpp"
#include "jht_ecs_callbackhub.hpp"
#include "jht_ecs_serviceprovider.hpp"

namespace jht::ecs {
    Registry::Registry(ServiceProvider* serviceProvider) : ServiceDependent(serviceProvider), _callbackHub(_serviceProvider->Get<CallbackHub>()) {}

    bool Registry::_add(Component* component)
    {
        std::u8string_view id = component->GetId();
        assert(!!id.length() && "Registry::_add Component id must not be empty!");
        _components.emplace(id, component);

        if(!!_callbackHub)
        {
            auto cast = dynamic_cast<IEventReceiver*>(component);
            if(!!cast)
            {
                _callbackHub->Register(cast);
            }
        }
        return true;
    }
    bool Registry::_remove(Component* component)
    {
        std::u8string_view id = component->GetId();
        assert(!!id.length() && "Registry::_add Component id must not be empty!");

        bool found = false;

        auto range = _components.equal_range(id);

        for(auto iter = range.first; iter != range.second; ++iter)
        {
            if(iter->second == component)
            {
                _components.erase(iter);
                found = true;
                break;
            }
        }

        if(!!_callbackHub && found)
        {
            auto cast = dynamic_cast<IEventReceiver*>(component);
            if(!!cast)
            {
                _callbackHub->Unregister(cast);
            }
        }
        return found;
    }

    Registry::~Registry()
    {
        if(!!_callbackHub)
        {
            for(auto pair : _components)
            {
                auto cast = dynamic_cast<IEventReceiver*>(pair.second);
                if(!!cast)
                {
                    _callbackHub->Unregister(cast);
                }
            }
        }
        _callbackHub = nullptr;

        for(auto pair : _components)
        {
            delete pair.second;
        }
        _components.clear();
    }

    Registry& Registry::SetCallbackHub(CallbackHub* callbackHub)
    {
        if(_callbackHub == callbackHub)
        {
            return *this;
        }
        if(!!_callbackHub)
        {
            for(auto pair : _components)
            {
                auto cast = dynamic_cast<IEventReceiver*>(pair.second);
                if(!!cast)
                {
                    _callbackHub->Unregister(cast);
                }
            }
        }
        _callbackHub = callbackHub;
        if(!!_callbackHub)
        {
            for(auto pair : _components)
            {
                auto cast = dynamic_cast<IEventReceiver*>(pair.second);
                if(!!cast)
                {
                    _callbackHub->Register(cast);
                }
            }
        }
        return *this;
    }
}  // namespace jht::ecs
