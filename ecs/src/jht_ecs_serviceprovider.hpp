#pragma once
#include "jht_ecs_declares.hpp"
#include "jht_ecs_service.hpp"
#include "jht_ecs_servicedependent.hpp"
#include <functional>
#include <jht_basics.hpp>
#include <map>
#include <mutex>
#include <stdexcept>
#include <type_traits>

namespace jht::ecs {
    /// @brief Class managing singletons
    class ServiceProvider
    {
      public:
        using ServiceBuilderFunc = std::function<IService*(ServiceProvider* serviceProvider)>;

        /// @brief Adds a service of type TService to the providers collection
        /// @param service A service, instantiated with new()
        template <typename TService>
        inline TService* AddSingleton(TService* service);

        /// @brief Adds a service of type TService to the providers collection
        template <typename TService>
        inline TService* AddSingleton();

        /// @brief Adds a service of type TService to the providers collection by providing a function to build it lazily
        inline void AddLazySingleton(std::u8string_view id, ServiceBuilderFunc& func);

        /// @brief Gets a service from the collection by type (does not work for lazy services!)
        template <typename TService>
        inline TService* Get();

        /// @brief Gets a service from the collection by id and type. Will throw runtime_error if a lazy service of matching id but non-matching type is constructed.
        template <typename TService>
        inline TService* Get(std::u8string_view id);

        /// @brief Gets a required service from the collection by type (does not work for lazy services!). Will throw runtime_error if service is not found
        template <typename TService>
        inline TService* GetRequired();

        /// @brief Gets a required service from the collection by id and type. Will throw runtime_error if a lazy service of matching id but non-matching type is constructed. Will throw runtime_error if service is not found
        template <typename TService>
        inline TService* GetRequired(std::u8string_view id);

        /// @brief Get a list of services instantiated and lazily available
        /// @return Count of services
        int64_t GetServiceNames(std::vector<std::u8string>& out_names) const;

        /// @brief Get all services castable to TService
        /// @return Count of services
        template <typename TService>
        inline int64_t GetAll(std::vector<TService*>& out_services);

        virtual ~ServiceProvider();

      protected:
        class ServiceInstantiation
        {
          public:
            IService* Service;
            uint64_t  Order;
        };

        using servicemap_t     = std::map<std::u8string_view, ServiceInstantiation>;
        using lazyservicemap_t = std::unordered_map<std::u8string_view, ServiceBuilderFunc>;

        mutable std::recursive_mutex _serviceMutex;
        servicemap_t                 _services = {};
        uint64_t                     _order    = 0;

        void _add(IService* service);

        mutable std::recursive_mutex _lazyMutex;
        lazyservicemap_t             _lazyServices = {};
    };

    template <typename TService>
    inline TService* ServiceProvider::AddSingleton(TService* service)
    {
        static_assert(std::is_base_of_v<IService, TService>, "Services need to inherit jht::ecs::IService class!");
        _add(service);
        return service;
    }

    template <typename TService>
    inline TService* ServiceProvider::AddSingleton()
    {
        static_assert(std::is_base_of_v<IService, TService>, "Services need to inherit jht::ecs::IService class!");
        TService* service = nullptr;
        if constexpr(std::is_base_of_v<ServiceDependent, TService>)
        {
            service = new TService(this);
        }
        else
        {
            service = new TService();
        }
        _add(service);
        return service;
    }

    template <typename TService>
    inline TService* ServiceProvider::Get()
    {
        static_assert(std::is_base_of_v<IService, TService>, "Services need to inherit jht::ecs::IService class!");
        std::lock_guard<std::recursive_mutex> lock(_serviceMutex);
        // Check instantiated services
        for(auto pair : _services)
        {
            TService* cast = dynamic_cast<TService*>(pair.second.Service);
            if(!!cast)
            {
                return cast;
            }
        }
        return nullptr;
    }

    template <typename TService>
    inline TService* ServiceProvider::GetRequired()
    {
        TService* service = Get<TService>();
        if(!service)
        {
            throw new std::runtime_error("Required service not found!");
        }
        return service;
    }

    template <typename TService>
    inline TService* ServiceProvider::Get(std::u8string_view id)
    {
        static_assert(std::is_base_of_v<IService, TService>, "Services need to inherit jht::ecs::IService class!");
        {  // Check instantiated services
            servicemap_t::iterator iter;
            {
                std::lock_guard<std::recursive_mutex> lock(_serviceMutex);
                iter = _services.find(id);
            }
            if(iter != _services.end())
            {
                return dynamic_cast<TService*>(iter->second);
            }
        }
        {  // Check lazy services
            lazyservicemap_t::iterator iter;
            {
                std::lock_guard<std::recursive_mutex> lock(_lazyMutex);
                iter = _lazyServices.find(id);
            }
            if(iter != _lazyServices.end())
            {
                const ServiceBuilderFunc& builder = iter->second;
                IService*                 service = std::invoke(builder, this);
                TService*                 cast    = dynamic_cast<TService*>(service);
                if(!cast)
                {
                    delete service;
                    throw std::runtime_error("A lazily constructed service was not castable to the required type!");
                }
                _add(service);
                return cast;
            }
        }
        return nullptr;
    }

    template <typename TService>
    inline TService* ServiceProvider::GetRequired(std::u8string_view id)
    {
        TService* service = Get<TService>(id);
        if(!service)
        {
            throw new std::runtime_error("Required service not found!");
        }
        return service;
    }

    template <typename TService>
    inline int64_t ServiceProvider::GetAll(std::vector<TService*>& out_services)
    {
        static_assert(std::is_base_of_v<IService, TService>, "Services need to inherit jht::ecs::IService class!");
        std::lock_guard<std::recursive_mutex> lock(_serviceMutex);

        int64_t startCount = static_cast<int64_t>(out_services.size());
        // Check instantiated services
        for(auto pair : _services)
        {
            TService* cast = dynamic_cast<TService*>(pair.second.Service);
            if(!!cast)
            {
                out_services.push_back(cast);
            }
        }
        return static_cast<int64_t>(out_services.size()) - startCount;
    }
}  // namespace jht::ecs
