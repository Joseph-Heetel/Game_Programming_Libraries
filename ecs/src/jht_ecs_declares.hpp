#pragma once

namespace jht::ecs {
    class Component;
    class ServiceProvider;
    class Registry;
    class IService;
    class CallbackHub;
    class ICallbackListBase;
    class IEventReceiver;
}  // namespace jht::ecs
