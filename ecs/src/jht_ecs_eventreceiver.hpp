#pragma once
#include "jht_ecs_declares.hpp"
#include <jht_basics.hpp>

namespace jht::ecs {
    class IEventReceiver
    {
      public:
        virtual ~IEventReceiver() {}  // virtual destructor assures polymorphism
    };
}  // namespace jht::ecs
