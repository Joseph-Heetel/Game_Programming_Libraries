#pragma once
#include "jht_ecs_declares.hpp"
#include "jht_ecs_event.hpp"
#include <jht_basics.hpp>

namespace jht::ecs {
    /// @brief Callback list base class providing non-typed add and remove methods
    class ICallbackListBase
    {
      public:
        /// @brief Adds component or service to the list if it implements the event listener
        virtual void Add(IEventReceiver* componentOrService) = 0;
        /// @brief Removes component or service from the list if it implements the event listener and is already in list
        virtual void Remove(IEventReceiver* componentOrService) = 0;
        virtual ~ICallbackListBase() {}
    };

    /// @brief Callback list base class adding typed invoke callback
    template <typename TEventListener>
    class ICallbackList : public ICallbackListBase
    {
      public:
        /// @brief Invokes the list by sequentially invoking all listeners
        virtual void Invoke(typename TEventListener::TArg arg) = 0;
    };
}  // namespace jht::ecs
