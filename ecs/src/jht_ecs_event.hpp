#pragma once
#include "jht_ecs_declares.hpp"
#include <jht_basics.hpp>

namespace jht::ecs {

    class IEventListenerBase
    {
      public:
        virtual std::u8string_view GetId() const = 0;
    };

    /// @brief Base class to override for event specifications
    /// @remark Override: std::u8string_view GetId() const, void Callback(TArg arg)
    template <typename TArg_>
    class IEventListener : public IEventListenerBase
    {
      public:
        using TArg                      = TArg_;
        virtual void Callback(TArg arg) = 0;
    };

    /*
        * EXAMPLE:
        *   
        *   // Class specifying the example event type: 
        *   class ExampleEvent : public IEventListener<int32_t>
        *   {
        *     public:
        *       // Any class wishing to listen to example events override this method
        *       virtual void OnExample(int32_t val) = 0;
        *       // Generalized callback which the callback list classes know to invoke
        *       virtual void Callback(TArg arg) { OnExample(arg); }
        *   };
        *   
        */

    /// @brief Base class for event specification with ordered invocation
    /// @remark Override: std::u8string_view GetId() const, void Callback(TArg arg)
    template <typename TArg_>
    class IOrderedEventListener : public IEventListener<TArg_>
    {
      public:
        virtual uint32_t GetOrder() const { return 1000U; }
    };
}  // namespace jht::ecs
