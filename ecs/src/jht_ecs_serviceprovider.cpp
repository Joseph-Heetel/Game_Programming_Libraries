#include "jht_ecs_serviceprovider.hpp"
#include "jht_ecs_callbackhub.hpp"
#include "jht_ecs_service.hpp"
#include <cassert>


namespace jht::ecs {
    void ServiceProvider::_add(IService* service)
    {
        CallbackHub* callbackhub = nullptr;
        {
            callbackhub = Get<CallbackHub>();
        }
        {
            std::u8string_view id = service->GetId();
            assert(!!id.length() && "IService id must not be empty!");
            std::lock_guard<std::recursive_mutex> lock(_serviceMutex);

            auto iter = _services.find(id);
            if(iter != _services.end())
            {
                delete iter->second.Service;
            }
            _services[id] = ServiceInstantiation{.Service = service, .Order = _order};
            _order++;
        }
        auto cast = reinterpret_cast<IEventReceiver*>(service);
        if(!!callbackhub && !!cast)
        {
            callbackhub->Register(cast);
        }
    }

    void ServiceProvider::AddLazySingleton(std::u8string_view nameview, ServiceBuilderFunc& func)
    {
        std::lock_guard<std::recursive_mutex> lock(_lazyMutex);
        std::u8string                         name(nameview);
        _lazyServices[name] = func;
    }


    ServiceProvider::~ServiceProvider()
    {
        CallbackHub* callbackhub = nullptr;
        {
            callbackhub = Get<CallbackHub>();
        }
        {
            std::lock_guard<std::recursive_mutex> lazylock(_lazyMutex);
            _lazyServices.clear();

            std::multimap<uint64_t, IService*, std::greater<uint64_t>> deleteOrder;

            std::lock_guard<std::recursive_mutex> servicelock(_serviceMutex);
            for(auto pair : _services)
            {
                auto cast = reinterpret_cast<IEventReceiver*>(pair.second.Service);
                if(!!callbackhub && !!cast)
                {
                    callbackhub->Unregister(cast);
                }
                deleteOrder.emplace(pair.second.Order, pair.second.Service);
            }
            _services.clear();
            for(auto pair : deleteOrder)
            {
                delete pair.second;
            }
        }
    }

    int64_t ServiceProvider::GetServiceNames(std::vector<std::u8string>& out_names) const
    {
        int64_t count = 0;
        {
            std::lock_guard<std::recursive_mutex> servicelock(_serviceMutex);
            for(auto pair : _services)
            {
                out_names.push_back(std::u8string(pair.first));
            }
            count += static_cast<int64_t>(_services.size());
        }
        {
            std::lock_guard<std::recursive_mutex> lazylock(_lazyMutex);
            for(auto pair : _lazyServices)
            {
                out_names.push_back(std::u8string(pair.first));
            }
            count += static_cast<int64_t>(_lazyServices.size());
        }
        return count;
    }
}  // namespace jht::ecs
