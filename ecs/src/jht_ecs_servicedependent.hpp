#pragma once
#include "jht_ecs_declares.hpp"
#include <jht_basics.hpp>

namespace jht::ecs {
    /// @brief Base class for classes which require a service provider to function
    class ServiceDependent
    {
      public:
        /// @brief Get service provider
        inline ServiceProvider* GetServiceProvider() { return _serviceProvider; }
        /// @brief Get service provider
        inline const ServiceProvider* GetServiceProvider() const { return _serviceProvider; }

        inline virtual ~ServiceDependent() { _serviceProvider = nullptr; }

      protected:
        ServiceDependent(ServiceProvider* provider);

        ServiceProvider* _serviceProvider = nullptr;
    };
}  // namespace jht::ecs
