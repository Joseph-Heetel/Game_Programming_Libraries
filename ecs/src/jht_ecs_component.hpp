#pragma once
#include "jht_ecs_declares.hpp"
#include "jht_ecs_eventreceiver.hpp"
#include "jht_ecs_servicedependent.hpp"
#include <jht_basics.hpp>

namespace jht::ecs {
    /// @brief Base class for any component
    class Component : public ServiceDependent, public IEventReceiver
    {
        friend Registry;

      public:
        Component(const Component& other)            = delete;
        Component(const Component&& other)           = delete;
        Component& operator=(const Component& other) = delete;

        /// @brief Stringview of id used for identification. Must be a compile time constant at time unique to this type. Qualified name of type suggested.
        virtual std::u8string_view GetId() const = 0;

        /// @brief Registry the component is assigned to. Guaranteed valid for object lifetime.
        inline Registry* GetRegistry() { return _registry; }
        /// @brief Registry the component is assigned to. Guaranteed valid for object lifetime.
        inline const Registry* GetRegistry() const { return _registry; }

        inline virtual ~Component() { _registry = nullptr; }

      protected:
        Component(Registry* registry);

        /// @brief Registry the component is assigned to. Guaranteed valid for object lifetime.
        Registry* _registry = nullptr;
    };
}  // namespace jht::ecs
