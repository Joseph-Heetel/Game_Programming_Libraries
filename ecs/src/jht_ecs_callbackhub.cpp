#include "jht_ecs_callbackhub.hpp"
#include "jht_ecs_serviceprovider.hpp"

namespace jht::ecs {
    CallbackHub::CallbackHub(ServiceProvider* serviceProvider) : ServiceDependent(serviceProvider), IService()
    {
        std::vector<ICallbackListConfig*> callbackListConfigs;
        _serviceProvider->GetAll<ICallbackListConfig>(callbackListConfigs);

        for(auto callbackListConfig : callbackListConfigs)
        {
            auto callbackLists = callbackListConfig->GetCallbackLists();
            for(auto callbackList : callbackLists)
            {
                _callbackLists.push_back(callbackList);
            }
        }
    }

    CallbackHub& CallbackHub::AddCallbackList(ICallbackListBase* callbackList)
    {
        _callbackLists.push_back(callbackList);
        return *this;
    }

    void CallbackHub::_add(IEventReceiver* listener)
    {
        for(auto& list : _callbackLists)
        {
            list->Add(listener);
        }
    }
    void CallbackHub::_remove(IEventReceiver* listener)
    {
        for(auto& list : _callbackLists)
        {
            list->Remove(listener);
        }
    }

    CallbackHub::~CallbackHub()
    {
        for(auto list : _callbackLists)
        {
            delete list;
        }
        _callbackLists.clear();
    }
}  // namespace jht::ecs
