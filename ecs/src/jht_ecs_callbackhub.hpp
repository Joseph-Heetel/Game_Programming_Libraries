#pragma once
#include "jht_ecs_callbacklistbase.hpp"
#include "jht_ecs_callbacklistconfig.hpp"
#include "jht_ecs_declares.hpp"
#include "jht_ecs_event.hpp"
#include "jht_ecs_service.hpp"
#include "jht_ecs_servicedependent.hpp"
#include <jht_basics.hpp>
#include <vector>

namespace jht::ecs {

    /// @brief Singleton service managing callback lists
    class CallbackHub : public ServiceDependent, public IService
    {
      public:
        inline virtual std::u8string_view GetId() const override { return u8"jht::ecs::CallbackHub"; }

        explicit CallbackHub(ServiceProvider* serviceProvider);

        /// @brief Invoke an events callbacklist
        template <typename TEvent>
        void Invoke(typename TEvent::TArg arg);

        /// @brief Won't affect already existing components! Add a callback list to the hub
        CallbackHub& AddCallbackList(ICallbackListBase* callbackList);

        virtual ~CallbackHub();

        void Register(IEventReceiver* listener) { _add(listener); }
        void Unregister(IEventReceiver* listener) { _remove(listener); }

      protected:
        std::vector<ICallbackListBase*> _callbackLists;

        void _add(IEventReceiver* listener);
        void _remove(IEventReceiver* listener);
    };

    template <typename TEvent>
    void CallbackHub::Invoke(typename TEvent::TArg arg)
    {
        for(auto list : _callbackLists)
        {
            auto callback = dynamic_cast<ICallbackList<TEvent>*>(list);
            if(!!callback)
            {
                callback->Invoke(arg);
            }
        }
    }

}  // namespace jht::ecs