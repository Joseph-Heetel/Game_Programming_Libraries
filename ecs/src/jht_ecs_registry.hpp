#pragma once
#include "jht_ecs_component.hpp"
#include "jht_ecs_declares.hpp"
#include "jht_ecs_servicedependent.hpp"
#include <cassert>
#include <jht_basics.hpp>
#include <map>
#include <vector>

namespace jht::ecs {
    class Registry : public ServiceDependent
    {
      public:
        Registry(ServiceProvider* serviceProvider);

        /// @brief Instantiates a new component
        template <typename TComponent>
        inline TComponent* MakeComponent();

        /// @brief Adds a manually instantiated component instance (initiate with new, Registry manages finalization)
        template <typename TComponent>
        inline void AddOrMoveComponent(TComponent* component);

        /// @brief Test wether a component matching type TComponent is registered
        template <typename TComponent>
        inline bool HasComponent() const;

        /// @brief Test wether a component matching type TComponent is registered, matching id
        template <typename TComponent>
        inline bool HasComponent(std::u8string_view id) const;

        /// @brief Gets first component that can be cast to TComponent type
        template <typename TComponent>
        inline TComponent* GetComponent();

        /// @brief Gets first component that can be cast to TComponent type
        template <typename TComponent>
        inline const TComponent* GetComponent() const;

        /// @brief Gets first component that can be cast to TComponent type, matching id
        template <typename TComponent>
        inline TComponent* GetComponent(std::u8string_view id);

        /// @brief Gets first component that can be cast to TComponent type, matching id
        template <typename TComponent>
        inline const TComponent* GetComponent(std::u8string_view id) const;

        /// @brief Appends all components which can be cast to TComponent type to the out vector
        template <typename TComponent>
        inline int32_t GetComponents(std::vector<TComponent*>& out);

        /// @brief Appends all components which can be cast to TComponent type to the out vector
        template <typename TComponent>
        inline int32_t GetComponents(std::vector<const TComponent*>& out) const;

        /// @brief Removes and finalizes a component
        inline bool RemoveDeleteComponent(Component* component);

        /// @brief Get Callback hub singleton managing callbacks for components registered
        inline CallbackHub* GetCallbackHub() { return _callbackHub; }
        /// @brief Get Callback hub singleton managing callbacks for components registered
        inline const CallbackHub* GetCallbackHub() const { return _callbackHub; }
        /// @brief Set Callback hub singleton managing callbacks for components registered
        Registry& SetCallbackHub(CallbackHub* callbackhub);

        virtual ~Registry();

      protected:
        CallbackHub* _callbackHub = nullptr;

        bool _add(Component* component);
        bool _remove(Component* component);

        std::multimap<std::u8string_view, Component*> _components;
    };

    template <typename TComponent>
    inline TComponent* Registry::MakeComponent()
    {
        TComponent* value = new TComponent(this);
        _add(value);
        return value;
    }

    template <typename TComponent>
    inline void Registry::AddOrMoveComponent(TComponent* component)
    {
        static_assert(std::is_base_of_v<Component, TComponent>, "Registry::AddComponent: TComponent type must inherit from jht::ecs::Component!");
        assert(component && "Registry::AddComponent: Parameter component is nullptr!");

        if(!!component->_registry)
        {
            component->_registry->_remove(component);
        }

        _add(component);
    }

    template <typename TComponent>
    inline bool Registry::HasComponent() const
    {
        const auto value = GetComponent<TComponent>();
        return !!value;
    }

    template <typename TComponent>
    inline bool Registry::HasComponent(std::u8string_view id) const
    {
        const auto value = GetComponent<TComponent>(id);
        return !!value;
    }

    template <typename TComponent>
    inline TComponent* Registry::GetComponent()
    {
        for(auto& pair : _components)
        {
            auto cast = dynamic_cast<TComponent*>(pair.second);
            if(!!cast)
            {
                return cast;
            }
        }
        return nullptr;
    }

    template <typename TComponent>
    inline const TComponent* Registry::GetComponent() const
    {
        for(const auto& pair : _components)
        {
            auto cast = dynamic_cast<TComponent*>(pair.second);
            if(!!cast)
            {
                return cast;
            }
        }
        return nullptr;
    }

    template <typename TComponent>
    inline TComponent* Registry::GetComponent(std::u8string_view id)
    {
        auto range = _components.equal_range(id);

        for(auto iter = range.first; iter != range.second; ++iter)
        {
            auto cast = dynamic_cast<TComponent*>(iter->second);
            if(!!cast)
            {
                return cast;
            }
        }
        return nullptr;
    }

    template <typename TComponent>
    inline const TComponent* Registry::GetComponent(std::u8string_view id) const
    {
        const auto range = _components.equal_range(id);

        for(auto iter = range.first; iter != range.second; ++iter)
        {
            auto cast = dynamic_cast<TComponent*>(iter->second);
            if(!!cast)
            {
                return cast;
            }
        }
        return nullptr;
    }

    template <typename TComponent>
    inline int32_t Registry::GetComponents(std::vector<TComponent*>& out)
    {
        int32_t writes = 0;
        for(auto& pair : _components)
        {
            auto cast = dynamic_cast<TComponent*>(pair.second);
            if(!!cast)
            {
                out.push_back(cast);
                writes++;
            }
        }
        return writes;
    }

    template <typename TComponent>
    inline int32_t Registry::GetComponents(std::vector<const TComponent*>& out) const
    {
        int32_t writes = 0;
        for(const auto& pair : _components)
        {
            auto cast = dynamic_cast<TComponent*>(pair.second);
            if(!!cast)
            {
                out.push_back(cast);
                writes++;
            }
        }
        return writes;
    }

    inline bool Registry::RemoveDeleteComponent(Component* component)
    {
        assert(!!component && "Registry::RemoveDeleteComponent: Parameter component is nullptr!");

        return _remove(component);
    }

}  // namespace jht::ecs
