#pragma once
#include "jht_ecs_callbacklistbase.hpp"
#include "jht_ecs_declares.hpp"
#include "jht_ecs_event.hpp"
#include <jht_basics.hpp>
#include <map>

namespace jht::ecs {

    /// @brief Callback implementation for ordered event listeners. Will invoke order values low -> high
    template <typename TEventListener>
    class OrderedCallbackList : public ICallbackList<TEventListener>
    {
      public:
        inline virtual void Add(IEventReceiver* listener) override;
        inline virtual void Remove(IEventReceiver* listener) override;

        virtual inline void Invoke(typename TEventListener::TArg arg) override;

        inline virtual ~OrderedCallbackList() { _listeners.clear(); }

      protected:
        std::multimap<uint32_t, TEventListener*> _listeners;
    };

    template <typename TEventListener>
    inline void OrderedCallbackList<TEventListener>::Add(IEventReceiver* listener)
    {
        auto cast = dynamic_cast<TEventListener*>(listener);

        if(!!cast)
        {
            _listeners.emplace(cast->GetOrder(), cast);
        }
    }

    template <typename TEventListener>
    inline void OrderedCallbackList<TEventListener>::Remove(IEventReceiver* listener)
    {
        auto cast = dynamic_cast<TEventListener*>(listener);

        if(!!cast)
        {
            auto range = _listeners.equal_range(cast->GetOrder());

            for(auto iter = range.first; iter != range.second; ++iter)
            {
                if(iter->second == cast)
                {
                    _listeners.erase(iter);
                    return;
                }
            }
        }
    }

    template <typename TEventListener>
    inline void OrderedCallbackList<TEventListener>::Invoke(typename TEventListener::TArg arg)
    {
        for(auto& pair : _listeners)
        {
            pair.second->Callback(arg);
        }
    }
}  // namespace jht::ecs
