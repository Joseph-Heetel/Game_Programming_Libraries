#pragma once
#include "jht_ecs_declares.hpp"
#include "jht_ecs_service.hpp"
#include <jht_basics.hpp>
#include <vector>

namespace jht::ecs {
    /// @brief Interface class to inherit for custom selections of callback lists
    /// @remark Override: std::u8string_view GetId() const, std::vector<ICallbackListBase*> GetCallbackLists() const
    class ICallbackListConfig : public IService
    {
      public:
        virtual std::vector<ICallbackListBase*> GetCallbackLists() const = 0;
    };
}  // namespace jht::ecs
