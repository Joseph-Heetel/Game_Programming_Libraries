#pragma once
#include "jht_ecs_declares.hpp"
#include "jht_ecs_eventreceiver.hpp"
#include <jht_basics.hpp>

namespace jht::ecs {
    /// @brief Non-static singleton base class
    class IService : public IEventReceiver
    {
      public:
        /// @brief Stringview of id used for identification. Must be a compile time constant at time unique to this type. Qualified name of type suggested.
        virtual std::u8string_view GetId() const = 0;

        inline virtual ~IService() {}

      protected:
        inline IService() {}
    };
}  // namespace jht::ecs
