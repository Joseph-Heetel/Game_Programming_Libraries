#include "jht_ecs_servicedependent.hpp"
#include <cassert>

namespace jht::ecs {
    ServiceDependent::ServiceDependent(ServiceProvider* provider) : _serviceProvider(provider)
    {
        assert(!!provider && "ServiceDependent: ServiceProvider must not be null!");
    }
}  // namespace jht::ecs
