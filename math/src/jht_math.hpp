#pragma once
#include "jht_math_mat4x4.hpp"
#include "jht_math_vec2.hpp"
#include "jht_math_vec3.hpp"
#include "jht_math_vec4.hpp"
#include "jht_math_vecext.hpp"
#include "jht_math_quat.hpp"
#include "jht_math_misc.hpp"
