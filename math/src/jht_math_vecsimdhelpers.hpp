#pragma once
#include "jht_math_declares.hpp"
#include "jht_math_vec2.hpp"
#include "jht_math_vec3.hpp"
#include "jht_math_vec4.hpp"
#include <jht_basics.hpp>

namespace jht::math {
    inline __m128 LoadM128(const Vec4& vec)
    {
        return _mm_load_ps(vec.Values);
    }
    inline __m128 LoadM128(const Vec3& vec)
    {
        return _mm_set_ps(0.f, vec.Values[2], vec.Values[1], vec.Values[0]);
    }
    inline __m128 LoadM128(const Vec2& vec)
    {
        return _mm_set_ps(0.f, 0.f, vec.Values[1], vec.Values[0]);
    }
    inline __m128 LoadM128(const Vec3& vec, const float substitute)
    {
        return _mm_set_ps(substitute, vec.Values[2], vec.Values[1], vec.Values[0]);
    }
    inline __m128 LoadM128(const Vec2& vec, const float substitute)
    {
        return _mm_set_ps(substitute, substitute, vec.Values[1], vec.Values[0]);
    }
    inline __m128 FastHorizontalAdd(__m128 v)
    {
        __m128 shuf = _mm_shuffle_ps(v, v, _MM_SHUFFLE(2, 3, 0, 1));
        __m128 sums = _mm_add_ps(v, shuf);
        shuf        = _mm_movehl_ps(shuf, sums);
        sums        = _mm_add_ss(sums, shuf);
        return sums;
    }

    template <uint32_t SIZE_>
    inline math::detail::Vector<fp32_t, SIZE_> FromM128(__m128 v)
    {
        float temp[4];
        _mm_storeu_ps(temp, v);
        return math::detail::Vector<fp32_t, SIZE_>(temp);
    }

    template <>
    inline Vec4 FromM128(__m128 v)
    {
        Vec4 result;
        _mm_store_ps(result.Values, v);
        return result;
    }
}  // namespace jht::math
