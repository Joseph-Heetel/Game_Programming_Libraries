#pragma once
#include "jht_math_declares.hpp"
#include <jht_basics.hpp>
#include <string.h>

namespace jht::math {
    namespace detail {
        template <typename COMP_>
        class Vector<COMP_, 3U>
        {
          protected:
            using VEC2 = Vector<COMP_, 2U>;

          public:
            using COMP                     = COMP_;
            static constexpr uint32_t SIZE = 3U;

            COMP Values[SIZE];

            /// @brief Construct with zeroed Values memory
            inline Vector() : Values{} {}
            /// @brief Construct with all components initialized to value 'v'
            inline explicit Vector(COMP v) : Values{v, v, v} {}

            constexpr Vector(const Vector<COMP, SIZE>& v)                        = default;
            constexpr Vector(Vector<COMP, SIZE>&& v)                             = default;
            constexpr Vector<COMP, SIZE>& operator=(const Vector<COMP, SIZE>& v) = default;
            ~Vector()                                                            = default;

            /// @brief Construct with individual components of 'v' converted via default static cast to COMP type
            template <typename TCOMP2_>
            inline constexpr explicit Vector(const Vector<TCOMP2_, SIZE>& v)
                : Values{static_cast<COMP>(v.Values[0]), static_cast<COMP>(v.Values[1]), static_cast<COMP>(v.Values[2])}
            {
            }

            /// @brief Initialize from array
            inline constexpr Vector(const COMP* v) : Values{v[0], v[1], v[2]} {}

            /// @brief Composite constructor
            inline constexpr Vector(COMP v0, COMP v1, COMP v2) : Values{v0, v1, v2} {}
            /// @brief Composite constructor
            inline constexpr Vector(const VEC2& v0, COMP v1) : Values{v0[0], v0[1], v1} {}
            /// @brief Composite constructor
            inline constexpr Vector(const COMP v0, VEC2& v1) : Values{v0, v1[0], v1[1]} {}

            inline constexpr COMP  operator[](size_t pos) const { return Values[pos]; }
            inline constexpr COMP& operator[](size_t pos) { return Values[pos]; }

            inline constexpr COMP  x() const { return Values[0]; }
            inline constexpr COMP& x() { return Values[0]; }
            inline constexpr COMP  y() const { return Values[1]; }
            inline constexpr COMP& y() { return Values[1]; }
            inline constexpr COMP  z() const { return Values[2]; }
            inline constexpr COMP& z() { return Values[2]; }
            inline constexpr COMP  r() const { return Values[0]; }
            inline constexpr COMP& r() { return Values[0]; }
            inline constexpr COMP  g() const { return Values[1]; }
            inline constexpr COMP& g() { return Values[1]; }
            inline constexpr COMP  b() const { return Values[2]; }
            inline constexpr COMP& b() { return Values[2]; }

            inline constexpr bool operator==(const Vector<COMP, SIZE>& v) const { return memcmp(Values, v.Values, sizeof(COMP) * SIZE) == 0; }
            inline constexpr bool operator!=(const Vector<COMP, SIZE>& v) const { return memcmp(Values, v.Values, sizeof(COMP) * SIZE) != 0; }

            inline constexpr Vector<COMP, SIZE>  operator+(const Vector<COMP, SIZE>& v) const { return Vector<COMP, SIZE>(Values[0] + v[0], Values[1] + v[1], Values[2] + v[2]); }
            inline constexpr Vector<COMP, SIZE>  operator-(const Vector<COMP, SIZE>& v) const { return Vector<COMP, SIZE>(Values[0] - v[0], Values[1] - v[1], Values[2] - v[2]); }
            inline constexpr Vector<COMP, SIZE>  operator*(const Vector<COMP, SIZE>& v) const { return Vector<COMP, SIZE>(Values[0] * v[0], Values[1] * v[1], Values[2] * v[2]); }
            inline constexpr Vector<COMP, SIZE>  operator/(const Vector<COMP, SIZE>& v) const { return Vector<COMP, SIZE>(Values[0] / v[0], Values[1] / v[1], Values[2] / v[2]); }
            inline constexpr Vector<COMP, SIZE>  operator+(COMP v) const { return Vector<COMP, SIZE>(Values[0] + v, Values[1] + v, Values[2] + v); }
            inline constexpr Vector<COMP, SIZE>  operator-(COMP v) const { return Vector<COMP, SIZE>(Values[0] - v, Values[1] - v, Values[2] - v); }
            inline constexpr Vector<COMP, SIZE>  operator*(COMP v) const { return Vector<COMP, SIZE>(Values[0] * v, Values[1] * v, Values[2] * v); }
            inline constexpr Vector<COMP, SIZE>  operator/(COMP v) const { return Vector<COMP, SIZE>(Values[0] / v, Values[1] / v, Values[2] / v); }
            inline constexpr Vector<COMP, SIZE>& operator+=(const Vector<COMP, SIZE>& v);
            inline constexpr Vector<COMP, SIZE>& operator-=(const Vector<COMP, SIZE>& v);
            inline constexpr Vector<COMP, SIZE>& operator*=(const Vector<COMP, SIZE>& v);
            inline constexpr Vector<COMP, SIZE>& operator/=(const Vector<COMP, SIZE>& v);
            inline constexpr Vector<COMP, SIZE>& operator+=(COMP v);
            inline constexpr Vector<COMP, SIZE>& operator-=(COMP v);
            inline constexpr Vector<COMP, SIZE>& operator*=(COMP v);
            inline constexpr Vector<COMP, SIZE>& operator/=(COMP v);
            inline constexpr Vector<COMP, SIZE>& operator++();
            inline constexpr Vector<COMP, SIZE>& operator--();
        };

#pragma region vec3 inline
        template <typename COMP_>
        inline constexpr Vector<COMP_, 3U>& Vector<COMP_, 3U>::operator+=(const Vector<COMP, SIZE>& v)
        {
            Values[0] += v[0];
            Values[1] += v[1];
            Values[2] += v[2];
            return *this;
        }
        template <typename COMP_>
        inline constexpr Vector<COMP_, 3U>& Vector<COMP_, 3U>::operator-=(const Vector<COMP, SIZE>& v)
        {
            Values[0] -= v[0];
            Values[1] -= v[1];
            Values[2] -= v[2];
            return *this;
        }
        template <typename COMP_>
        inline constexpr Vector<COMP_, 3U>& Vector<COMP_, 3U>::operator*=(const Vector<COMP, SIZE>& v)
        {
            Values[0] *= v[0];
            Values[1] *= v[1];
            Values[2] *= v[2];
            return *this;
        }
        template <typename COMP_>
        inline constexpr Vector<COMP_, 3U>& Vector<COMP_, 3U>::operator/=(const Vector<COMP, SIZE>& v)
        {
            Values[0] /= v[0];
            Values[1] /= v[1];
            Values[2] /= v[2];
            return *this;
        }
        template <typename COMP_>
        inline constexpr Vector<COMP_, 3U>& Vector<COMP_, 3U>::operator+=(COMP v)
        {
            Values[0] += v;
            Values[1] += v;
            Values[2] += v;
            return *this;
        }
        template <typename COMP_>
        inline constexpr Vector<COMP_, 3U>& Vector<COMP_, 3U>::operator-=(COMP v)
        {
            Values[0] -= v;
            Values[1] -= v;
            Values[2] -= v;
            return *this;
        }
        template <typename COMP_>
        inline constexpr Vector<COMP_, 3U>& Vector<COMP_, 3U>::operator*=(COMP v)
        {
            Values[0] *= v;
            Values[1] *= v;
            Values[2] *= v;
            return *this;
        }
        template <typename COMP_>
        inline constexpr Vector<COMP_, 3U>& Vector<COMP_, 3U>::operator/=(COMP v)
        {
            Values[0] /= v;
            Values[1] /= v;
            Values[2] /= v;
            return *this;
        }
        template <typename COMP_>
        inline constexpr Vector<COMP_, 3U>& Vector<COMP_, 3U>::operator++()
        {
            Values[0]++;
            Values[1]++;
            Values[2]++;
            return *this;
        }
        template <typename COMP_>
        inline constexpr Vector<COMP_, 3U>& Vector<COMP_, 3U>::operator--()
        {
            Values[0]--;
            Values[1]--;
            Values[2]--;
            return *this;
        }
#pragma endregion
    }  // namespace detail

    using Vec3  = detail::Vector<fp32_t, 3U>;
    using iVec3 = detail::Vector<int32_t, 3U>;
    using uVec3 = detail::Vector<uint32_t, 3U>;
}  // namespace jht::math
