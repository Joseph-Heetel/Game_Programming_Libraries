#pragma once
#include "jht_math_declares.hpp"
#include "jht_math_vec2.hpp"
#include "jht_math_vec3.hpp"
#include "jht_math_vec4.hpp"
#include "jht_math_vecsimdhelpers.hpp"
#include <jht_basics.hpp>
#include <math.h>

namespace jht::math {
    template <typename COMP_, uint32_t SIZE_>
    inline constexpr COMP_ DotC(const math::detail::Vector<COMP_, SIZE_>& l, const math::detail::Vector<COMP_, SIZE_>& r)
    {
        COMP_ dot = l.Values[0] * r.Values[0];
        if constexpr(SIZE_ > 1U)
            dot += l.Values[1] * r.Values[1];
        if constexpr(SIZE_ > 2U)
            dot += l.Values[2] * r.Values[2];
        if constexpr(SIZE_ > 3U)
            dot += l.Values[3] * r.Values[3];
        return dot;
    }

    template <typename COMP_, uint32_t SIZE_>
    inline COMP_ Dot(const math::detail::Vector<COMP_, SIZE_>& l, const math::detail::Vector<COMP_, SIZE_>& r)
    {
        COMP_ dot = l.Values[0] * r.Values[0];
        if constexpr(SIZE_ > 1U)
            dot += l.Values[1] * r.Values[1];
        if constexpr(SIZE_ > 2U)
            dot += l.Values[2] * r.Values[2];
        if constexpr(SIZE_ > 3U)
            dot += l.Values[3] * r.Values[3];
        return dot;
    }

    template <>
    inline fp32_t Dot(const math::detail::Vector<fp32_t, 4U>& l, const math::detail::Vector<fp32_t, 4U>& r)
    {
        float out[4];
        _mm_storeu_ps(out, LoadM128(l) * LoadM128(r));
        return out[0] + out[1] + out[2] + out[3];
    }

    template <>
    inline fp32_t Dot(const math::detail::Vector<fp32_t, 3U>& l, const math::detail::Vector<fp32_t, 3U>& r)
    {
        float out[4];
        _mm_storeu_ps(out, LoadM128(l) * LoadM128(r));
        return out[0] + out[1] + out[2];
    }

    template <uint32_t SIZE_>
    inline fp32_t Length(const math::detail::Vector<fp32_t, SIZE_>& v)
    {
        return sqrt(Dot(v));
    }

    template <uint32_t SIZE_>
    inline constexpr fp32_t LengthC(const math::detail::Vector<fp32_t, SIZE_>& v)
    {
        return sqrt(DotC(v));
    }

    template <typename COMP_, uint32_t SIZE_>
    inline COMP_ LengthSquared(const math::detail::Vector<COMP_, SIZE_>& v)
    {
        return Dot(v, v);
    }

    template <typename COMP_, uint32_t SIZE_>
    inline constexpr COMP_ LengthSquaredC(const math::detail::Vector<COMP_, SIZE_>& v)
    {
        return DotC(v, v);
    }

    template <uint32_t SIZE_>
    inline math::detail::Vector<fp32_t, SIZE_> Normalize(const math::detail::Vector<fp32_t, SIZE_>& v)
    {
        auto vec      = LoadM128(v);
        auto invSqRt  = _mm_rsqrt_ss(FastHorizontalAdd(vec * vec));
        auto shuffled = _mm_shuffle_ps(invSqRt, invSqRt, _MM_SHUFFLE(0, 0, 0, 0));
        return FromM128<SIZE_>(vec * shuffled);
    }
}  // namespace jht::math
