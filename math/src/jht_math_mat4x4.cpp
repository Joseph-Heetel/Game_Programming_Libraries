#include "jht_math_mat4x4.hpp"
#include <cmath>

namespace jht::math {
    namespace detail {
        /// @brief Calculates dot product of one vector with four other vectors simultaneously
        /// @remarks Consider Vec4 a and Vec4[4] b. Vec4 out contains [ a · b[0] |  a · b[1] | a · b[2] | a · b[3] ]
        /// @param a four component vector
        /// @param b array of four vectors with 4 components each
        /// @param temp array of temporary result vectors
        /// @param out four component vector for the output
        inline void SIMD_DotProductColumn(const float* a, __m128 b[4], __m128& out)
        {
            out = (_mm_set1_ps(a[0]) * b[0]) + (_mm_set1_ps(a[1]) * b[1]) + (_mm_set1_ps(a[2]) * b[2]) + (_mm_set1_ps(a[3]) * b[3]);
        }

        template <int32_t t0, int32_t t1, int32_t t2, int32_t t3>
        inline constexpr int32_t ShuffleMask()
        {
            return t0 | (t1 << 2) | (t2 << 4) | (t3 << 6);
        }

        template <int32_t t0, int32_t t1, int32_t t2, int32_t t3>
        inline __m128 Shuffle(__m128 v)
        {
            return _mm_castsi128_ps(_mm_shuffle_epi32(_mm_castps_si128(v), (ShuffleMask<t0, t1, t2, t3>())));
        }

    }  // namespace impl

    bool Mat4x4::Inverse(Mat4x4& result) const
    {
        // ala glm / mesa (basically just sub matrix calc, adjuncts, determinant)

        float inv[16], det;

        inv[0] = Values[5] * Values[10] * Values[15] - Values[5] * Values[11] * Values[14] - Values[9] * Values[6] * Values[15] + Values[9] * Values[7] * Values[14]
                 + Values[13] * Values[6] * Values[11] - Values[13] * Values[7] * Values[10];

        inv[4] = -Values[4] * Values[10] * Values[15] + Values[4] * Values[11] * Values[14] + Values[8] * Values[6] * Values[15] - Values[8] * Values[7] * Values[14]
                 - Values[12] * Values[6] * Values[11] + Values[12] * Values[7] * Values[10];

        inv[8] = Values[4] * Values[9] * Values[15] - Values[4] * Values[11] * Values[13] - Values[8] * Values[5] * Values[15] + Values[8] * Values[7] * Values[13]
                 + Values[12] * Values[5] * Values[11] - Values[12] * Values[7] * Values[9];

        inv[12] = -Values[4] * Values[9] * Values[14] + Values[4] * Values[10] * Values[13] + Values[8] * Values[5] * Values[14] - Values[8] * Values[6] * Values[13]
                  - Values[12] * Values[5] * Values[10] + Values[12] * Values[6] * Values[9];

        inv[1] = -Values[1] * Values[10] * Values[15] + Values[1] * Values[11] * Values[14] + Values[9] * Values[2] * Values[15] - Values[9] * Values[3] * Values[14]
                 - Values[13] * Values[2] * Values[11] + Values[13] * Values[3] * Values[10];

        inv[5] = Values[0] * Values[10] * Values[15] - Values[0] * Values[11] * Values[14] - Values[8] * Values[2] * Values[15] + Values[8] * Values[3] * Values[14]
                 + Values[12] * Values[2] * Values[11] - Values[12] * Values[3] * Values[10];

        inv[9] = -Values[0] * Values[9] * Values[15] + Values[0] * Values[11] * Values[13] + Values[8] * Values[1] * Values[15] - Values[8] * Values[3] * Values[13]
                 - Values[12] * Values[1] * Values[11] + Values[12] * Values[3] * Values[9];

        inv[13] = Values[0] * Values[9] * Values[14] - Values[0] * Values[10] * Values[13] - Values[8] * Values[1] * Values[14] + Values[8] * Values[2] * Values[13]
                  + Values[12] * Values[1] * Values[10] - Values[12] * Values[2] * Values[9];

        inv[2] = Values[1] * Values[6] * Values[15] - Values[1] * Values[7] * Values[14] - Values[5] * Values[2] * Values[15] + Values[5] * Values[3] * Values[14]
                 + Values[13] * Values[2] * Values[7] - Values[13] * Values[3] * Values[6];

        inv[6] = -Values[0] * Values[6] * Values[15] + Values[0] * Values[7] * Values[14] + Values[4] * Values[2] * Values[15] - Values[4] * Values[3] * Values[14]
                 - Values[12] * Values[2] * Values[7] + Values[12] * Values[3] * Values[6];

        inv[10] = Values[0] * Values[5] * Values[15] - Values[0] * Values[7] * Values[13] - Values[4] * Values[1] * Values[15] + Values[4] * Values[3] * Values[13]
                  + Values[12] * Values[1] * Values[7] - Values[12] * Values[3] * Values[5];

        inv[14] = -Values[0] * Values[5] * Values[14] + Values[0] * Values[6] * Values[13] + Values[4] * Values[1] * Values[14] - Values[4] * Values[2] * Values[13]
                  - Values[12] * Values[1] * Values[6] + Values[12] * Values[2] * Values[5];

        inv[3] = -Values[1] * Values[6] * Values[11] + Values[1] * Values[7] * Values[10] + Values[5] * Values[2] * Values[11] - Values[5] * Values[3] * Values[10]
                 - Values[9] * Values[2] * Values[7] + Values[9] * Values[3] * Values[6];

        inv[7] = Values[0] * Values[6] * Values[11] - Values[0] * Values[7] * Values[10] - Values[4] * Values[2] * Values[11] + Values[4] * Values[3] * Values[10]
                 + Values[8] * Values[2] * Values[7] - Values[8] * Values[3] * Values[6];

        inv[11] = -Values[0] * Values[5] * Values[11] + Values[0] * Values[7] * Values[9] + Values[4] * Values[1] * Values[11] - Values[4] * Values[3] * Values[9]
                  - Values[8] * Values[1] * Values[7] + Values[8] * Values[3] * Values[5];

        inv[15] = Values[0] * Values[5] * Values[10] - Values[0] * Values[6] * Values[9] - Values[4] * Values[1] * Values[10] + Values[4] * Values[2] * Values[9]
                  + Values[8] * Values[1] * Values[6] - Values[8] * Values[2] * Values[5];

        det = Values[0] * inv[0] + Values[1] * inv[4] + Values[2] * inv[8] + Values[3] * inv[12];

        if(det == 0)
            return false;

        det = 1.f / det;

        __m128 detsimd = _mm_set1_ps(det);

        __m128 resultvec[4];

        for(int32_t i = 0; i < 16; i += 4)
        {
            resultvec[i] = _mm_loadu_ps(inv + i) * det;
        }

        result.StoreM128Column<0>(resultvec[0]);
        result.StoreM128Column<1>(resultvec[1]);
        result.StoreM128Column<2>(resultvec[2]);
        result.StoreM128Column<3>(resultvec[3]);

        return true;
    }


    Vec4 Mat4x4::operator*(const Vec4& v) const
    {
        using namespace jht::math::detail;

        __m128 acc{_mm_setzero_ps()};  // output matrix columns

        __m128 lCols[4];  // column vectors of the left hand matrix
        LoadM128Columns(lCols);
        const fp32_t* rCol = v.Values;  // single column vector of the right hand matrix

        // For each input column, calculate the output column by performing a packed dot product calculation

        SIMD_DotProductColumn(rCol, lCols, acc);  // Column 0
        return FromM128<4>(acc);
    }

    Mat4x4 Mat4x4::operator*(const Mat4x4& v) const
    {
        using namespace jht::math::detail;

        __m128 acc[4]{_mm_setzero_ps(), _mm_setzero_ps(), _mm_setzero_ps(), _mm_setzero_ps()};  // output matrix columns

        __m128 lCols[4];  // column vectors of the left hand matrix
        LoadM128Columns(lCols);
        const fp32_t* rCol = v.Values;  // single column vector of the right hand matrix

        // For each input column, calculate the output column by performing a packed dot product calculation

        SIMD_DotProductColumn(rCol, lCols, acc[0]);  // Column 0
        rCol += 4;
        SIMD_DotProductColumn(rCol, lCols, acc[1]);  // Column 1
        rCol += 4;
        SIMD_DotProductColumn(rCol, lCols, acc[2]);  // Column 2
        rCol += 4;
        SIMD_DotProductColumn(rCol, lCols, acc[3]);  // Column 3

        return Mat4x4(acc);
    }

    Mat4x4 Mat4x4::Translate(const Vec3& v)
    {
        Mat4x4 result(1.f);  // Identity matrix
        memcpy(result.Values + 12, v.Values, sizeof(float) * 3);
        return result;
    }

    Mat4x4 Mat4x4::Translate(const Vec4& v)
    {
        Mat4x4 result(1.f);  // Identity matrix
        memcpy(result.Values + 12, v.Values, sizeof(float) * 3);
        return result;
    }

    Mat4x4 Mat4x4::Rotate(const Quat& q)
    {
        Mat4x4 result(1.f);

        fp32_t qxx(q.x() * q.x());
        fp32_t qyy(q.y() * q.y());
        fp32_t qzz(q.z() * q.z());
        fp32_t qxz(q.x() * q.z());
        fp32_t qxy(q.x() * q.y());
        fp32_t qyz(q.y() * q.z());
        fp32_t qwx(q.w() * q.x());
        fp32_t qwy(q.w() * q.y());
        fp32_t qwz(q.w() * q.z());

        result.At(0, 0) = 1.f - 2.f * (qyy + qzz);
        result.At(0, 1) = 2.f * (qxy + qwz);
        result.At(0, 2) = 2.f * (qxz - qwy);

        result.At(1, 0) = 2.f * (qxy - qwz);
        result.At(1, 1) = 1.f - 2.f * (qxx + qzz);
        result.At(1, 2) = 2.f * (qyz + qwx);

        result.At(2, 0) = 2.f * (qxz + qwy);
        result.At(2, 1) = 2.f * (qyz - qwx);
        result.At(2, 2) = 1.f - 2.f * (qxx + qyy);
        return result;
    }

    Mat4x4 Mat4x4::Scale(const Vec3& v)
    {
        Mat4x4 result(1.f);  // Identity matrix
        result.At(0, 0) = v[0];
        result.At(1, 1) = v[1];
        result.At(2, 2) = v[2];
        return result;
    }

    Mat4x4 Mat4x4::Scale(const Vec4& v)
    {
        Mat4x4 result;
        result.At(0, 0) = v[0];
        result.At(1, 1) = v[1];
        result.At(2, 2) = v[2];
        result.At(3, 3) = v[3];
        return result;
    }

    Mat4x4 Mat4x4::Perspective(fp32_t verticalFov, fp32_t aspect, fp32_t near, fp32_t far, Mat4x4* out_inverse)
    {
        float focal_length = 1.0f / tan(verticalFov / 2.0f);

        float x = focal_length / aspect;
        float y = -focal_length;
        float A = near / (far - near);
        float B = far * A;

        Mat4x4 projection(x, 0.0f, 0.0f, 0.0f, 0.0f, y, 0.0f, 0.0f, 0.0f, 0.0f, A, B, 0.0f, 0.0f, -1.0f, 0.0f);

        if(!!out_inverse)
        {
            new(out_inverse) Mat4x4(1 / x, 0.0f, 0.0f, 0.0f, 0.0f, 1 / y, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1 / B, A / B);
        }

        return projection;
    }

}  // namespace jht::math
