#pragma once
#include "jht_math_declares.hpp"
#include "jht_math_quat.hpp"
#include "jht_math_vec4.hpp"
#include "jht_math_vecsimdhelpers.hpp"
#include <immintrin.h>
#include <jht_basics.hpp>

namespace jht::math {

    class Mat4x4
    {
      public:
        static constexpr uint32_t SIZE = 16U;

        alignas(16) fp32_t Values[SIZE];  // column major

        /*
                Values layout:
                 /  00  04  08  12  \
                |   01  05  09  13   |
                |   02  06  10  14   |
                 \  03  07  11  15  /
            */

        /// @brief Initializes as identity matrix
        inline constexpr explicit Mat4x4() : Values{1.f, 0.f, 0.f, 0.f, 0.f, 1.f, 0.f, 0.f, 0.f, 0.f, 1.f, 0.f, 0.f, 0.f, 0.f, 1.f} {}
        /// @brief Initializes with v on the major diagonal
        inline constexpr explicit Mat4x4(fp32_t v) : Values{v, 0.f, 0.f, 0.f, 0.f, v, 0.f, 0.f, 0.f, 0.f, v, 0.f, 0.f, 0.f, 0.f, v} {}
        /// @brief Initializes from vectors as columns
        inline constexpr Mat4x4(const math::Vec4& v0, const math::Vec4& v1, const math::Vec4& v2, const math::Vec4& v3) {}
        /// @brief Initializes from array (column major)
        inline Mat4x4(const fp32_t* v) : Values{} { memcpy(Values, v, sizeof(fp32_t) * SIZE); }

        /// @brief Initializes from individual components (column major)
        inline constexpr Mat4x4(fp32_t v0,
                                fp32_t v1,
                                fp32_t v2,
                                fp32_t v3,
                                fp32_t v4,
                                fp32_t v5,
                                fp32_t v6,
                                fp32_t v7,
                                fp32_t v8,
                                fp32_t v9,
                                fp32_t v10,
                                fp32_t v11,
                                fp32_t v12,
                                fp32_t v13,
                                fp32_t v14,
                                fp32_t v15)
            : Values{v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14, v15}
        {
        }

        /// @brief Initializes from vector of four __m128 column vectors
        inline Mat4x4(const __m128* v);

        /// @brief Access components (column major)
        inline constexpr fp32_t operator[](size_t pos) const { return Values[pos]; }
        /// @brief Access components (column major)
        inline constexpr fp32_t& operator[](size_t pos) { return Values[pos]; }
        /// @brief Access components
        inline constexpr fp32_t At(size_t column, size_t row) const { return Values[column * 4 + row]; }
        /// @brief Access components
        inline constexpr fp32_t& At(size_t column, size_t row) { return Values[column * 4 + row]; }

        inline constexpr math::Vec4 ColVec(size_t column) const { return math::Vec4(Values + column * 4); }
        inline void                 ColVec(size_t column, math::Vec4& out) const { memcpy(out.Values, Values + column * 4, sizeof(fp32_t) * 4); }

        /// @brief Returns a transposed copy of this
        inline constexpr Mat4x4 Transposed() const;

        bool Inverse(Mat4x4& result) const;

        /// @brief Load column designated by template parameter 'column' (0 based index) into __m128
        template <uint32_t column>
        inline __m128 LoadM128Column() const;
        /// @brief Load column (0 based index) into __m128
        inline __m128 LoadM128Column(const size_t column) const { return _mm_load_ps(Values + 4 * column); }
        /// @brief Load row designated by template parameter 'row' (0 based index) into __m128
        template <uint32_t row>
        inline __m128 LoadM128Row() const;
        /// @brief Load row (0 based index) into __m128
        inline __m128 LoadM128Row(const size_t row) const;
        /// @brief Load row designated by template parameter 'row' (0 based index) into __m128. Use 'rowbuf' temporary buffer.
        /// @brief Load matrix rows into four __m128 row vectors
        inline void LoadM128Rows(__m128 rows[4]) const;
        /// @brief Load matrix columns into four __m128 column vectors
        inline void LoadM128Columns(__m128 columns[4]) const;
        /// @brief Store __m128 into column designated by template parameter 'column' (0 based index)
        template <uint32_t column>
        inline void StoreM128Column(const __m128& v);

        inline bool operator==(const Mat4x4& v) const;

        /// @brief Calculates the matrix vector product (vector = matrix * vector) leveraging SIMD instructions
        math::Vec4 operator*(const math::Vec4& v) const;

        /// @brief Calculates the matrix matrix product (matrix = matrix * matrix) leveraging SIMD instructions
        Mat4x4 operator*(const Mat4x4& v) const;

        /// @brief Calculates a translation matrix
        static Mat4x4 Translate(const math::Vec3& v);
        /// @brief Calculates a translation matrix
        static Mat4x4 Translate(const math::Vec4& v);
        /// @brief Calculates a rotation matrix
        static Mat4x4 Rotate(const Quat& q);
        /// @brief Calculates a scale matrix
        static Mat4x4 Scale(const math::Vec3& v);
        /// @brief Calculates a scale matrix
        static Mat4x4 Scale(const math::Vec4& v);

        /// @brief Calculates a perspective matrix
        /// @param verticalFov Vertical field of view in radians
        /// @param aspect Aspect ratio calculated as width / height
        /// @param near Near plane
        /// @param far Far plane
        /// @param out_inverse Optionally provides the inverse of the matrix
        static Mat4x4 Perspective(fp32_t verticalFov, fp32_t aspect, fp32_t near, fp32_t far, Mat4x4* out_inverse = nullptr);
        /// @brief Calculates a perspective matrix
        /// @param verticalFov Vertical field of view in radians
        /// @param width Width of the render target
        /// @param height Height of the render target
        /// @param near Near plane
        /// @param far Far plane
        /// @param out_inverse Optionally provides the inverse of the matrix
        static inline Mat4x4 Perspective(fp32_t verticalFov, fp32_t width, fp32_t height, fp32_t near, fp32_t far, Mat4x4* out_inverse = nullptr)
        {
            return Perspective(verticalFov, width / height, near, far, out_inverse);
        }
    };

    inline Mat4x4::Mat4x4(const __m128* v)
    {
        StoreM128Column<0>(v[0]);
        StoreM128Column<1>(v[1]);
        StoreM128Column<2>(v[2]);
        StoreM128Column<3>(v[3]);
    }

    inline constexpr Mat4x4 Mat4x4::Transposed() const
    {
        return Mat4x4(Values[0], Values[4], Values[8], Values[12], Values[1], Values[5], Values[9], Values[13], Values[2], Values[6], Values[10], Values[14], Values[3], Values[7],
                      Values[11], Values[15]);
    }

    template <uint32_t column>
    inline __m128 Mat4x4::LoadM128Column() const
    {
        return _mm_load_ps(Values + 4 * column);
    }

    template <uint32_t row>
    inline __m128 Mat4x4::LoadM128Row() const
    {
        return _mm_set_ps(Values[12 + row], Values[8 + row], Values[4 + row], Values[0 + row]);  // this stupid function expects args in reversed order for some reason lmao
    }

    inline __m128 Mat4x4::LoadM128Row(const size_t row) const
    {
        return _mm_set_ps(Values[12 + row], Values[8 + row], Values[4 + row], Values[0 + row]);  // this stupid function expects args in reversed order for some reason lmao
    }

    inline void Mat4x4::LoadM128Rows(__m128 rows[4]) const
    {
        rows[0] = LoadM128Row<0>();
        rows[1] = LoadM128Row<1>();
        rows[2] = LoadM128Row<2>();
        rows[3] = LoadM128Row<3>();
    }

    inline void Mat4x4::LoadM128Columns(__m128 columns[4]) const
    {
        columns[0] = LoadM128Column<0>();
        columns[1] = LoadM128Column<1>();
        columns[2] = LoadM128Column<2>();
        columns[3] = LoadM128Column<3>();
    }

    template <uint32_t column>
    inline void Mat4x4::StoreM128Column(const __m128& v)
    {
        _mm_store_ps(Values + 4 * column, v);
    }

    inline bool Mat4x4::operator==(const Mat4x4& v) const
    {
        return memcmp(Values, v.Values, sizeof(fp32_t) * SIZE) == 0;
    }

    using Mat4 = Mat4x4;
}  // namespace jht::math
