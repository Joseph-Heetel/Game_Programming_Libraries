#pragma once
#include "jht_math_declares.hpp"
#include <jht_basics.hpp>
#include <string.h>

namespace jht::math {
    namespace detail {
        template <typename COMP_>
        class Vector<COMP_, 2U>
        {
          public:
            using COMP                     = COMP_;
            static constexpr uint32_t SIZE = 2U;

            COMP Values[SIZE];

            /// @brief Construct with zeroed Values memory
            inline constexpr Vector() : Values{} {}
            /// @brief Construct with all components initialized to value 'v'
            inline constexpr explicit Vector(COMP v) : Values{v, v} {}

            constexpr Vector(const Vector<COMP, SIZE>& v)                        = default;
            constexpr Vector(Vector<COMP, SIZE>&& v)                             = default;
            constexpr Vector<COMP, SIZE>& operator=(const Vector<COMP, SIZE>& v) = default;
            ~Vector()                                                            = default;

            /// @brief Construct with individual components of 'v' converted via default static cast to COMP type
            template <typename TCOMP2_>
            inline explicit constexpr Vector(const Vector<TCOMP2_, SIZE>& v) : Values{static_cast<COMP>(v.Values[0]), static_cast<COMP>(v.Values[1])}
            {
            }

            /// @brief Initialize from array
            inline constexpr Vector(const COMP* v) : Values{v[0], v[1]} {}

            /// @brief Composite constructor
            inline constexpr Vector(COMP v0, COMP v1) : Values{v0, v1} {}

            inline constexpr COMP  operator[](size_t pos) const { return Values[pos]; }
            inline constexpr COMP& operator[](size_t pos) { return Values[pos]; }

            inline constexpr COMP  x() const { return Values[0]; }
            inline constexpr COMP& x() { return Values[0]; }
            inline constexpr COMP  y() const { return Values[1]; }
            inline constexpr COMP& y() { return Values[1]; }
            inline constexpr COMP  r() const { return Values[0]; }
            inline constexpr COMP& r() { return Values[0]; }
            inline constexpr COMP  g() const { return Values[1]; }
            inline constexpr COMP& g() { return Values[1]; }

            inline constexpr bool operator==(const Vector<COMP, SIZE>& v) const { return memcmp(Values, v.Values, sizeof(COMP) * SIZE) == 0; }
            inline constexpr bool operator!=(const Vector<COMP, SIZE>& v) const { return memcmp(Values, v.Values, sizeof(COMP) * SIZE) != 0; }

            inline constexpr Vector<COMP, SIZE>  operator+(const Vector<COMP, SIZE>& v) const { return Vector<COMP, SIZE>(Values[0] + v[0], Values[1] + v[1]); }
            inline constexpr Vector<COMP, SIZE>  operator-(const Vector<COMP, SIZE>& v) const { return Vector<COMP, SIZE>(Values[0] - v[0], Values[1] - v[1]); }
            inline constexpr Vector<COMP, SIZE>  operator*(const Vector<COMP, SIZE>& v) const { return Vector<COMP, SIZE>(Values[0] * v[0], Values[1] * v[1]); }
            inline constexpr Vector<COMP, SIZE>  operator/(const Vector<COMP, SIZE>& v) const { return Vector<COMP, SIZE>(Values[0] / v[0], Values[1] / v[1]); }
            inline constexpr Vector<COMP, SIZE>  operator+(COMP v) const { return Vector<COMP, SIZE>(Values[0] + v, Values[1] + v); }
            inline constexpr Vector<COMP, SIZE>  operator-(COMP v) const { return Vector<COMP, SIZE>(Values[0] - v, Values[1] - v); }
            inline constexpr Vector<COMP, SIZE>  operator*(COMP v) const { return Vector<COMP, SIZE>(Values[0] * v, Values[1] * v); }
            inline constexpr Vector<COMP, SIZE>  operator/(COMP v) const { return Vector<COMP, SIZE>(Values[0] / v, Values[1] / v); }
            inline constexpr Vector<COMP, SIZE>& operator+=(const Vector<COMP, SIZE>& v);
            inline constexpr Vector<COMP, SIZE>& operator-=(const Vector<COMP, SIZE>& v);
            inline constexpr Vector<COMP, SIZE>& operator*=(const Vector<COMP, SIZE>& v);
            inline constexpr Vector<COMP, SIZE>& operator/=(const Vector<COMP, SIZE>& v);
            inline constexpr Vector<COMP, SIZE>& operator+=(COMP v);
            inline constexpr Vector<COMP, SIZE>& operator-=(COMP v);
            inline constexpr Vector<COMP, SIZE>& operator*=(COMP v);
            inline constexpr Vector<COMP, SIZE>& operator/=(COMP v);
            inline constexpr Vector<COMP, SIZE>& operator++();
            inline constexpr Vector<COMP, SIZE>& operator--();
        };

#pragma region vec2 inline
        template <typename COMP_>
        inline constexpr Vector<COMP_, 2U>& Vector<COMP_, 2U>::operator+=(const Vector<COMP, SIZE>& v)
        {
            Values[0] += v[0];
            Values[1] += v[1];
            return *this;
        }
        template <typename COMP_>
        inline constexpr Vector<COMP_, 2U>& Vector<COMP_, 2U>::operator-=(const Vector<COMP, SIZE>& v)
        {
            Values[0] -= v[0];
            Values[1] -= v[1];
            return *this;
        }
        template <typename COMP_>
        inline constexpr Vector<COMP_, 2U>& Vector<COMP_, 2U>::operator*=(const Vector<COMP, SIZE>& v)
        {
            Values[0] *= v[0];
            Values[1] *= v[1];
            return *this;
        }
        template <typename COMP_>
        inline constexpr Vector<COMP_, 2U>& Vector<COMP_, 2U>::operator/=(const Vector<COMP, SIZE>& v)
        {
            Values[0] /= v[0];
            Values[1] /= v[1];
            return *this;
        }
        template <typename COMP_>
        inline constexpr Vector<COMP_, 2U>& Vector<COMP_, 2U>::operator+=(COMP v)
        {
            Values[0] += v;
            Values[1] += v;
            return *this;
        }
        template <typename COMP_>
        inline constexpr Vector<COMP_, 2U>& Vector<COMP_, 2U>::operator-=(COMP v)
        {
            Values[0] -= v;
            Values[1] -= v;
            return *this;
        }
        template <typename COMP_>
        inline constexpr Vector<COMP_, 2U>& Vector<COMP_, 2U>::operator*=(COMP v)
        {
            Values[0] *= v;
            Values[1] *= v;
            return *this;
        }
        template <typename COMP_>
        inline constexpr Vector<COMP_, 2U>& Vector<COMP_, 2U>::operator/=(COMP v)
        {
            Values[0] /= v;
            Values[1] /= v;
            return *this;
        }
        template <typename COMP_>
        inline constexpr Vector<COMP_, 2U>& Vector<COMP_, 2U>::operator++()
        {
            Values[0]++;
            Values[1]++;
            return *this;
        }
        template <typename COMP_>
        inline constexpr Vector<COMP_, 2U>& Vector<COMP_, 2U>::operator--()
        {
            Values[0]--;
            Values[1]--;
            return *this;
        }
#pragma endregion

    }  // namespace detail

    using Vec2  = detail::Vector<fp32_t, 2U>;
    using iVec2 = detail::Vector<int32_t, 2U>;
    using uVec2 = detail::Vector<uint32_t, 2U>;
}  // namespace jht::math
