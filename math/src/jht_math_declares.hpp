#pragma once
#include <immintrin.h>
#include <jht_basics.hpp>
#include <string.h>

namespace jht::math::detail {
    /// @brief Vector class
    template <typename COMP_, uint32_t SIZE_>
    class Vector;
}  // namespace jht::math::detail