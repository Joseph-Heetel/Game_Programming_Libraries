#pragma once
#include <cmath>
#include <jht_basics.hpp>

namespace jht::math {
    inline constexpr fp32_t PI     = 3.14159265358979323846264f;
    inline constexpr fp32_t TWOPI  = 2 * PI;
    inline constexpr fp32_t HALFPI = 0.5f * PI;

    inline constexpr fp32_t ToRadians(fp32_t degrees)
    {
        return degrees * (PI / 180.f);
    }

    inline constexpr fp32_t ToDegrees(fp32_t radians)
    {
        return radians * (180.f / PI);
    }
}  // namespace jht::math
