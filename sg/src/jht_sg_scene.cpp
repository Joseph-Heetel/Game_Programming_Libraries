#include "jht_sg_scene.hpp"
#include "jht_sg_events.hpp"
#include "jht_sg_gameobject.hpp"
#include "jht_sg_transform.hpp"

namespace jht::sg {
    Scene::Scene(ecs::ICallbackListConfig* eventconfig)
    {
        if(!!eventconfig)
        {
            _serviceProvider.AddSingleton(eventconfig);
        }
        else
        {
            _serviceProvider.AddSingleton<SceneEvents>();
        }
        _serviceProvider.AddSingleton<ecs::CallbackHub>();
    }

    GameObject* Scene::MakeGameObject(GameObject* parent)
    {
        assert((!parent || parent->_scene == this) && "Scene::MakeGameObject: Cannot assign parent to gameobject of different scene!");
        GameObject* gameobject = new GameObject(this);
        _gameObjectStorage.emplace(gameobject);
        Transform* transform = gameobject->GetTransform();
        if(!!parent)
        {
            Transform* parentTransform = parent->GetTransform();
            parentTransform->_children.push_back(transform);
            transform->_parent = parentTransform;
        }
        else
        {
            _root.push_back(gameobject);
        }
        return gameobject;
    }
    void Scene::MoveGameObject(GameObject* object, GameObject* newparent)
    {
        assert(object->_scene == this && "Scene::MoveGameObject: Cannot access gameobject attached to a different scene!");
        assert((!newparent || newparent->_scene == this) && "Scene::MoveGameObject: Cannot assign parent to gameobject of different scene!");
        Transform* transform          = object->GetTransform();
        Transform* oldParentTransform = transform->GetParent();
        if(!!oldParentTransform)
        {
            auto& oldParentChildren = oldParentTransform->GetChildren();
            auto  iter              = std::find(oldParentChildren.begin(), oldParentChildren.end(), transform);
            if(iter != oldParentChildren.end())
            {
                oldParentChildren.erase(iter);
            }
        }
        else
        {
            auto iter = std::find(_root.begin(), _root.end(), object);
            if(iter != _root.end())
            {
                _root.erase(iter);
            }
        }
        if(!!newparent)
        {
            Transform* newParentTransform = newparent->GetTransform();
            transform->_parent            = newParentTransform;
            newParentTransform->_children.push_back(transform);
        }
        else
        {
            _root.push_back(object);
            transform->_parent = nullptr;
        }
    }
    void Scene::DestroyGameObject(GameObject* object)
    {
        assert(object->_scene == this && "Scene::DestroyGameObject: Cannot access gameobject attached to a different scene!");

        object->_components.clear();

        Transform* transform          = object->GetTransform();
        Transform* oldParentTransform = transform->GetParent();
        if(!!oldParentTransform)
        {
            auto& oldParentChildren = oldParentTransform->GetChildren();
            auto  iter              = std::find(oldParentChildren.begin(), oldParentChildren.end(), transform);
            if(iter != oldParentChildren.end())
            {
                oldParentChildren.erase(iter);
            }
        }
        else
        {
            auto iter = std::find(_root.begin(), _root.end(), object);
            if(iter != _root.end())
            {
                _root.erase(iter);
            }
        }
        _gameObjectStorage.erase(object);
        delete object;
    }

}  // namespace jht::sg
