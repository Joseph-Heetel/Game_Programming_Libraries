#pragma once
#include <jht_basics.hpp>
#include <jht_ecs_callbackhub.hpp>
#include <jht_ecs_orderedcallbacklist.hpp>

namespace jht::sg {
    class UpdateEventArgs
    {
        fp32_t Delta = 0.f;
    };

    class UpdateEvent : public ecs::IOrderedEventListener<UpdateEventArgs&>
    {
      public:
        inline virtual uint32_t GetOrder() const override { return 1000U; }
        virtual void            OnUpdate(TArg args) = 0;
        inline virtual void     Callback(TArg args) override { OnUpdate(args); }
    };

    class SceneEvents : public ecs::ICallbackListConfig
    {
      public:
        virtual std::u8string_view                   GetId() const { return u8"jht::sg::SceneEvents"; }
        virtual std::vector<ecs::ICallbackListBase*> GetCallbackLists() const { return {new ecs::OrderedCallbackList<UpdateEvent>()}; }
    };
}  // namespace jht::sg
