#pragma once
#include "jht_sg_declares.hpp"
#include <jht_basics.hpp>
#include <jht_ecs_registry.hpp>


namespace jht::sg {
    /// @brief Gameobject provides access to scene and a registry for gameobject components to be attached to
    class GameObject : public ecs::Registry
    {
        friend Scene;

      public:
        /// @brief Get gameobjects name
        inline std::u8string_view GetName() const { return _name; }
        /// @brief Set gameobjects name
        inline GameObject& SetName(std::u8string_view name);

        inline Transform*       GetTransform() { return _transform; }
        inline const Transform* GetTransform() const { return _transform; }
        inline Scene*           GetScene() { return _scene; }
        inline const Scene*     GetScene() const { return _scene; }

      protected:
        GameObject(Scene* scene);
        virtual ~GameObject() {}

        std::u8string _name;
        Scene*        _scene;
        Transform*    _transform;
    };

    inline GameObject& GameObject::SetName(std::u8string_view name)
    {
        _name = name;
        return *this;
    }
}  // namespace jht::sg
