#include "jht_sg_gameobjectcomponent.hpp"
#include "jht_sg_gameobject.hpp"
#include <cassert>

namespace jht::sg {
    GameObjectComponent::GameObjectComponent(ecs::Registry* registry) : ecs::Component(registry)
    {
        assert(!!dynamic_cast<GameObject*>(registry) && "GameObjectComponent: Must be initialized with a GameObject registry!");
    }

    GameObject* GameObjectComponent::GetGameObject()
    {
        return reinterpret_cast<GameObject*>(_registry);
    }
    const GameObject* GameObjectComponent::GetGameObject() const
    {
        return reinterpret_cast<const GameObject*>(_registry);
    }
    Transform* GameObjectComponent::GetTransform()
    {
        return GetGameObject()->GetTransform();
    }
    const Transform* GameObjectComponent::GetTransform() const
    {
        return GetGameObject()->GetTransform();
    }
    Scene* GameObjectComponent::GetScene()
    {
        return GetGameObject()->GetScene();
    }
    const Scene* GameObjectComponent::GetScene() const
    {
        return GetGameObject()->GetScene();
    }

}  // namespace jht::sg
