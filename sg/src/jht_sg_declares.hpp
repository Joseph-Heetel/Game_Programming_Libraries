#pragma once

namespace jht::sg {
    class Scene;
    class GameObject;
    class GameObjectComponent;
    class Transform;
}  // namespace jht::sg
