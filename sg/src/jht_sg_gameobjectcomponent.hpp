#pragma once
#include "jht_sg_declares.hpp"
#include <jht_basics.hpp>
#include <jht_ecs_component.hpp>

namespace jht::sg {
    class GameObjectComponent : public ecs::Component
    {
      public:
        explicit GameObjectComponent(ecs::Registry* registry);

        GameObject*       GetGameObject();
        const GameObject* GetGameObject() const;
        Transform*        GetTransform();
        const Transform*  GetTransform() const;
        Scene*            GetScene();
        const Scene*      GetScene() const;
    };
}  // namespace jht::sg
