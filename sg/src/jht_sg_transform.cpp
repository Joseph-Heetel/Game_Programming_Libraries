#include "jht_sg_transform.hpp"

namespace jht::sg {
    void Transform::SetTranslation(const math::Vec3& translation)
    {
        SetDirtyRecursively();
        _translation = translation;
    }
    void Transform::SetRotation(const math::Quat& rotation)
    {
        SetDirtyRecursively();
        _rotation = rotation;
    }
    void Transform::SetScale(const math::Vec3& scale)
    {
        SetDirtyRecursively();
        _scale = scale;
    }

    const math::Mat4& Transform::GetLocalTransformMatrix()
    {
        if(_dirty)
        {
            Recalculate();
        }
        return _localTransform;
    }
    const math::Mat4& Transform::GetGlobalTransformMatrix()
    {
        if(_dirty)
        {
            Recalculate();
        }
        return _globalTransform;
    }

    void Transform::Recalculate()
    {
        _dirty          = false;
        _localTransform = math::Mat4x4::Translate(_translation) * math::Mat4x4::Rotate(_rotation) * math::Mat4x4::Scale(_scale);

        if(!!_parent)
        {
            _globalTransform = _localTransform * _parent->GetGlobalTransformMatrix();
        }
    }

    void Transform::SetDirtyRecursively()
    {
        if(_dirty)
        {
            return;
        }
        _dirty = true;
        for(auto child : _children)
        {
            child->SetDirtyRecursively();
        }
    }
}  // namespace jht::sg
