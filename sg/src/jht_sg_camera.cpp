#include "jht_sg_camera.hpp"
#include "jht_sg_transform.hpp"

namespace jht::sg {
    void Camera::RecalculateMatrices()
    {
        Transform* transform = GetTransform();
        if(!!transform)
        {
            _viewInverse = transform->GetGlobalTransformMatrix();
            _viewInverse.Inverse(_viewMat);
        }
        else
        {
            _viewMat     = math::Mat4(1.f);
            _viewInverse = math::Mat4(1.f);
        }

        _projMat = math::Mat4::Perspective(_verticalFov, _aspect, _near, _far, &_projInverse);
    }
}  // namespace jht::sg
