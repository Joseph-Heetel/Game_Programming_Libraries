#pragma once
#include "jht_sg_declares.hpp"
#include "jht_sg_gameobject.hpp"
#include <jht_basics.hpp>
#include <jht_ecs_callbackhub.hpp>
#include <jht_ecs_serviceprovider.hpp>
#include <memory>
#include <unordered_set>

namespace jht::sg {
    /// @brief Describes a scene consisting of a scenegraph of Gameobjects
    class Scene
    {
      public:
        explicit Scene(ecs::ICallbackListConfig* eventconfig = nullptr);

        /// @brief Get the scene specific service provider
        ecs::ServiceProvider* GetServiceProvider() { return &_serviceProvider; }

        /// @brief Construct a new gameobject
        /// @param parent If set to a gameobject, constructs it as child of that gameobject, otherwise constructs as root node
        GameObject* MakeGameObject(GameObject* parent = nullptr);
        /// @brief Moves a gameobject to a different parent (or root)
        void MoveGameObject(GameObject* object, GameObject* newparent = nullptr);
        /// @brief Destroys a gameobject
        void DestroyGameObject(GameObject* object);

      protected:
        ecs::ServiceProvider _serviceProvider;

        std::unordered_set<GameObject*> _gameObjectStorage;
        std::vector<GameObject*>        _root;
    };
}  // namespace jht::sg
