#pragma once
#include "jht_sg_gameobjectcomponent.hpp"
#include <jht_basics.hpp>
#include <jht_math.hpp>
#include <vector>

namespace jht::sg {
    /// @brief Component determining a position in world space
    class Transform : public GameObjectComponent
    {
        friend Scene;

      public:
        virtual std::u8string_view GetId() const override { return u8"jht::sg::Transform"; }
        inline Transform(ecs::Registry* gameObject) : GameObjectComponent(gameObject) {}

        /// @brief Gets the transforms parent node. May return null (indicating the node is root level)
        inline Transform* GetParent() { return _parent; }
        /// @brief Gets the transforms parent node. May return null (indicating the node is root level)
        inline const Transform* GetParent() const { return _parent; }
        /// @brief Gets child transforms attached to the node
        inline std::vector<Transform*>& GetChildren() { return _children; }
        /// @brief Gets child transforms attached to the node
        inline const std::vector<Transform*>& GetChildren() const { return _children; }
        /// @brief Get translation (relative to parent node)
        inline const math::Vec3& GetTranslation() const { return _translation; }
        /// @brief Get rotation (quaternion, relative to parent node)
        inline const math::Quat& GetRotation() const { return _rotation; }
        /// @brief Get scale (relative to parent node)
        inline const math::Vec3& GetScale() const { return _scale; }
        /// @brief Set translation relative to parent node.
        /// @remark Sets the node and all child nodes dirty.
        void SetTranslation(const math::Vec3& translation);
        /// @brief Set rotation relative to parent node
        /// @remark Sets the node and all child nodes dirty.
        void SetRotation(const math::Quat& rotation);
        /// @brief Set scale relative to parent node
        /// @remark Sets the node and all child nodes dirty.
        void SetScale(const math::Vec3& scale);
        /// @brief Gets local transform matrix
        /// @remark Recalculates recursively until non=dirty
        const math::Mat4& GetLocalTransformMatrix();
        /// @brief Gets global transform matrix
        /// @remark Recalculates recursively until non=dirty
        const math::Mat4& GetGlobalTransformMatrix();

      protected:
        /// @brief Calculates local and global transform. Fetching global transform from parent my cause recalculation recursively
        void Recalculate();
        /// @brief Sets the node and all its children dirty
        void SetDirtyRecursively();

        Transform*              _parent = nullptr;
        std::vector<Transform*> _children;

        bool _dirty = false;

        math::Vec3 _translation = math::Vec3();
        math::Quat _rotation    = math::Quat();
        math::Vec3 _scale       = math::Vec3(1.f);

        math::Mat4 _localTransform  = math::Mat4(1.f);
        math::Mat4 _globalTransform = math::Mat4(1.f);
    };
}  // namespace jht::sg
