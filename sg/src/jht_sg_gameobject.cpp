#include "jht_sg_gameobject.hpp"
#include "jht_sg_scene.hpp"
#include "jht_sg_transform.hpp"

namespace jht::sg {
    GameObject::GameObject(Scene* scene) : ecs::Registry(scene->GetServiceProvider()), _name(u8"Unnamed Game Object"), _scene(scene), _transform(MakeComponent<Transform>()) {}
}  // namespace jht::sg
