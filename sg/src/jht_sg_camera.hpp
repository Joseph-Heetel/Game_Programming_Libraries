#pragma once
#include "jht_sg_gameobjectcomponent.hpp"
#include <jht_basics.hpp>
#include <jht_math.hpp>

namespace jht::sg {
    /// @brief Component for calculating view and projection matrices
    class Camera : public GameObjectComponent
    {
      public:
        virtual std::u8string_view GetId() const override { return u8"jht::sg::Camera"; }
        inline Camera(ecs::Registry* gameObject) : GameObjectComponent(gameObject) {}

        /// @brief Calculate view matrix based on the transforms global transformation matrix and the projection matrix based on the projection properties
        virtual void RecalculateMatrices();

        /// @brief Get the view matrix
        virtual inline const math::Mat4& GetViewMatrix() const { return _viewMat; }
        /// @brief Get the projection matrix
        virtual inline const math::Mat4& GetProjectionMatrix() const { return _projMat; }
        /// @brief Get the inverse of the view matrix
        virtual inline const math::Mat4& GetInverseViewMatrix() const { return _viewInverse; }
        /// @brief Get the inverse of the projection matrix
        virtual inline const math::Mat4& GetInverseProjectionMatrix() const { return _projInverse; }
        /// @brief Get the vertical fov (radians)
        virtual inline fp32_t GetVerticalFov() const { return _verticalFov; }
        /// @brief Set the vertical fov
        /// @param verticalFov Vertical fov value in radians
        virtual inline Camera& SetVerticalFov(fp32_t verticalFov);
        /// @brief Get the aspect ratio (width / height)
        virtual inline fp32_t GetAspect() const { return _aspect; }
        /// @brief Set the aspect ratio
        /// @param aspect Aspect ratio calculated as such: width / height
        virtual inline Camera& SetAspect(fp32_t aspect);
        /// @brief Set the aspect ratio
        virtual inline Camera& SetAspect(fp32_t width, fp32_t height);
        /// @brief Get the near plane
        virtual inline fp32_t GetNear() const { return _near; }
        /// @brief Set the near plane
        virtual inline Camera& SetNear(fp32_t near);
        /// @brief Get the far plane
        virtual inline fp32_t GetFar() const { return _far; }
        /// @brief Set the far plane
        virtual inline Camera& SetFar(fp32_t far);

      protected:
        math::Mat4 _viewMat     = math::Mat4(1.f);
        math::Mat4 _viewInverse = math::Mat4(1.f);
        math::Mat4 _projMat     = math::Mat4(1.f);
        math::Mat4 _projInverse = math::Mat4(1.f);
        fp32_t     _verticalFov = math::ToRadians(80.f);
        fp32_t     _aspect      = 16.f / 9.f;
        fp32_t     _near        = 0.1f;
        fp32_t     _far         = 10000.f;
    };

    inline Camera& Camera::SetVerticalFov(fp32_t verticalFov)
    {
        _verticalFov = verticalFov;
        return *this;
    }

    inline Camera& Camera::SetAspect(fp32_t aspect)
    {
        _aspect = aspect;
        return *this;
    }

    inline Camera& Camera::SetAspect(fp32_t width, fp32_t height)
    {
        _aspect = width / height;
        return *this;
    }

    inline Camera& Camera::SetNear(fp32_t near)
    {
        _near = near;
        return *this;
    }
    inline Camera& Camera::SetFar(fp32_t far)
    {
        _far = far;
        return *this;
    }
}  // namespace jht::sg
