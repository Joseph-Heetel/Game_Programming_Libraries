#pragma once
#include <stdint.h>
#include <string>

namespace jht {
    using fp32_t = float;
    using fp64_t = double;

}  // namespace jht