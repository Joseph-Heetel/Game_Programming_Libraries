Collection of simple libraries providing utility for game engine programming.

## Entity Component System

See [ecs/readme.md](./ecs/readme.md)

## Scenegraph

See [sg/readme.md](./sg/readme.md)

## Vulkan

WIP - Will feature some simplifications to aid in development of rasterized graphics with vulkan

## Application

WIP - Will feature classes to manage application lifetime and os interaction through SDL and c++ standard library

## Math

WIP - Simple library to perform geometric operations in 3d space