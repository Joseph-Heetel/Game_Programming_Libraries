#pragma once
#include "jht_os_declares.hpp"
#include <jht_basics.hpp>

struct SDL_Window;

namespace jht::os {
    class Window
    {
        friend OsInterface;

      public:
        using Handle = SDL_Window*;

        /// @brief See SDL WindowFlags
        enum class EFlags : uint64_t
        {
            FullScreen        = 0x00000001,
            OpenGl            = 0x00000002,
            Shown             = 0x00000004,
            Hidden            = 0x00000008,
            Borderless        = 0x00000010,
            Resizable         = 0x00000020,
            Minimized         = 0x00000040,
            Maximized         = 0x00000080,
            MouseGrabbed      = 0x00000100,
            InputFocus        = 0x00000200,
            MouseFocus        = 0x00000400,
            FullScreenDesktop = (FullScreen | 0x00001000),
            Foreign           = 0x00000800,
            AllowHighDpi      = 0x00002000,
            MouseCapture      = 0x00004000,
            AlwaysOnTop       = 0x00008000,
            SkipTaskbar       = 0x00010000,
            Utility           = 0x00020000,
            Tooltip           = 0x00040000,
            PopupMenu         = 0x00080000,
            KeyboardGrabbed   = 0x00100000,
            Vulkan            = 0x10000000,
            Metal             = 0x20000000,
        };

        struct CreateInfo
        {
            std::u8string_view Name     = u8"jht::os::Window";
            int32_t            W        = 1280;
            int32_t            H        = 720;
            int32_t            X        = 0;
            int32_t            Y        = 0;
            bool               Centered = false;
            bool               Auto     = false;
            uint64_t           Flags    = 0;

            inline static CreateInfo Pos(std::u8string_view name, int32_t w, int32_t h, int32_t x, int32_t y, uint64_t flags)
            {
                return CreateInfo{.Name = name, .W = w, .H = h, .X = x, .Y = y, .Centered = false, .Auto = false, .Flags = flags};
            }

            inline static CreateInfo PosAuto(std::u8string_view name, int32_t w, int32_t h, uint64_t flags)
            {
                return CreateInfo{.Name = name, .W = w, .H = h, .X = 0, .Y = 0, .Centered = false, .Auto = true, .Flags = flags};
            }

            inline static CreateInfo PosCentered(std::u8string_view name, int32_t w, int32_t h, uint64_t flags)
            {
                return CreateInfo{.Name = name, .W = w, .H = h, .X = 0, .Y = 0, .Centered = true, .Auto = true, .Flags = flags};
            }
        };

        inline bool Exists() const { return _windowHandle != 0; }

        Window&                   SetTitle(std::u8string_view name);
        inline std::u8string_view GetTitle() const { return _name; }

        Window& SetSize(int32_t w, int32_t h);
        void    GetSize(int32_t& w, int32_t& h) const;

        Window& SetPositionAutoUndefined();
        Window& SetPositionAutoCentered();
        Window& SetPosition(int32_t x, int32_t y);
        void    GetPosition(int32_t& x, int32_t& y) const;

        Window& Hide();
        Window& Maximize();
        Window& Minimize();
        Window& BringToFront();
        Window& Restore();

        enum class EFlash
        {
            Cancel,
            Briefly,
            UntilFocus
        };

        Window& SetFlash(EFlash flash);

      protected:
        explicit Window(const CreateInfo& ci);
        virtual ~Window();

        Handle        _windowHandle = nullptr;
        std::u8string _name;
    };
}  // namespace jht::os
