#pragma once

namespace jht::os {
    class OsInterface;
    class Window;
    class InputDevice;
    class InputComponent;
    class ButtonInputComponent;
    class AxisInputComponent;
    class DirectionalInputComponent;
    class EventManager;

}  // namespace jht::os