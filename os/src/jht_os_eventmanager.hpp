#pragma once
#include <jht_basics.hpp>
#include <jht_ecs_callbacklistconfig.hpp>
#include <jht_ecs_service.hpp>
#include <jht_ecs_servicedependent.hpp>

namespace jht::os {
    class EventManager : public ecs::ServiceDependent, public ecs::ICallbackListConfig
    {
      public:
        inline virtual std::u8string_view GetId() const override { return u8"jht::os::EventManager"; }

        EventManager(ecs::ServiceProvider* serviceProvider);

        virtual bool PollAndDistribute();
        virtual std::vector<ecs::ICallbackListBase*> GetCallbackLists() const override;

      protected:
        ecs::CallbackHub* _callbackHub;
    };
}  // namespace jht::os
