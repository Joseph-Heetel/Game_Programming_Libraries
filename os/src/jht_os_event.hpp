#pragma once
#include "jht_os_declares.hpp"
#include "jht_os_inputlabels.hpp"
#include <jht_ecs_event.hpp>

namespace jht::os::event {
    class Base
    {
      public:
        inline explicit Base(uint64_t timestamp) : Timestamp(timestamp) {}
        uint64_t Timestamp;

        inline virtual ~Base() {}
    };

    class BaseListener : public ecs::IEventListener<const Base*>
    {
      public:
        inline virtual std::u8string_view GetId() const { return u8"jht::os::event::BaseListener"; }                                                                       \
        inline virtual void               Callback(const Base* arg) { OnOsEvent(arg); }                                                                                                    \
        inline virtual void               OnOsEvent(const Base* arg) {}                                                                                                                    \
    };

    namespace detail {
        class WindowBase
        {
          public:
            explicit WindowBase(Window* window);
            Window* SourceWindow;
        };

        class InputBase
        {
          public:
            explicit InputBase(InputDevice* inputDevice);
            InputDevice* SourceInputDevice;
        };

        class ButtonBase
        {
          public:
            ButtonBase(ButtonInputComponent* inputComponent, bool pressed);
            ButtonInputComponent* SourceInputComponent;
            bool                  Pressed;
        };

        class AxisBase
        {
          public:
            AxisBase(AxisInputComponent* inputComponent, fp64_t state);
            AxisInputComponent* SourceInputComponent;
            fp64_t              State;
        };

        class DirectionalBase
        {
          public:
            DirectionalBase(DirectionalInputComponent* inputComponent, int32_t deltaX, int32_t deltaY);
            DirectionalInputComponent* SourceInputComponent;
            int32_t                    DeltaX;
            int32_t                    DeltaY;
        };

        class MouseMovedBase
        {
          public:
            MouseMovedBase(int32_t posX, int32_t posY, int32_t deltaX, int32_t deltaY);
            int32_t PosX;
            int32_t PosY;
            int32_t DeltaX;
            int32_t DeltaY;
        };

        class DevicesChangedBase
        {
          public:
            DevicesChangedBase(InputDevice* added, InputDevice* removed);
            InputDevice* Added;
            InputDevice* Removed;
        };

        class ResizedBase
        {
          public:
            ResizedBase(uint32_t width, uint32_t height);
            uint32_t Width;
            uint32_t Height;
        };

        class FocusBase
        {
          public:
          FocusBase(bool mouseFocus, bool inputFocus);
          bool MouseFocus;
          bool InputFocus;
        };
    }  // namespace detail

#define JHT_OS_U8(a) u8##a
#define JHT_OS_STR(a) JHT_OS_U8(#a)
#define JHT_OS_STRCONCAT(a, b) JHT_OS_STR(a) JHT_OS_STR(b)

#define JHT_OS_DEFINE_EVENTLISTENER(name)                                                                                                                                          \
    class name##Listener : public ecs::IEventListener<const name*>                                                                                                                 \
    {                                                                                                                                                                              \
      public:                                                                                                                                                                      \
        inline virtual std::u8string_view GetId() const { return JHT_OS_STRCONCAT(jht::os::event::, name); }                                                                       \
        inline virtual void               Callback(TArg arg) { On##name(arg); }                                                                                                    \
        inline virtual void               On##name(TArg arg) {}                                                                                                                    \
    };

    class InputButton : public Base, public detail::InputBase, public detail::ButtonBase
    {
      public:
        InputButton(uint64_t timestamp, InputDevice*, ButtonInputComponent*, bool pressed);
    };
    JHT_OS_DEFINE_EVENTLISTENER(InputButton)

    class InputAxis : public Base, public detail::InputBase, public detail::AxisBase
    {
      public:
        InputAxis(uint64_t timestamp, InputDevice*, AxisInputComponent*, fp64_t state);
    };
    JHT_OS_DEFINE_EVENTLISTENER(InputAxis)

    class InputDirectional : public Base, public detail::InputBase, public detail::DirectionalBase
    {
      public:
        InputDirectional(uint64_t timestamp, InputDevice*, DirectionalInputComponent*, int32_t deltaX, int32_t deltaY);
    };
    JHT_OS_DEFINE_EVENTLISTENER(InputDirectional)

    class InputDevicesChanged : public Base, public detail::DevicesChangedBase
    {
      public:
        InputDevicesChanged(uint64_t timestamp, InputDevice* added, InputDevice* removed);
    };
    JHT_OS_DEFINE_EVENTLISTENER(InputDevicesChanged)

    class WindowResized : public Base, public detail::WindowBase, public detail::ResizedBase
    {
      public:
        WindowResized(uint64_t timestamp, Window*, uint32_t width, uint32_t height);
    };
    JHT_OS_DEFINE_EVENTLISTENER(WindowResized)

    class WindowFocus : public Base, public detail::WindowBase, public detail::FocusBase
    {
      public:
        WindowFocus(uint64_t timestamp, Window*, bool mouseFocus, bool windowFocus);
    };
    JHT_OS_DEFINE_EVENTLISTENER(WindowFocus)

    class WindowCloseRequest : public Base, public detail::WindowBase
    {
      public:
        WindowCloseRequest(uint64_t timestamp, Window*);
    };
    JHT_OS_DEFINE_EVENTLISTENER(WindowCloseRequest)


#undef JHT_OS_U8
#undef JHT_OS_STR
#undef JHT_OS_STRCONCAT
#undef JHT_OS_DEFINE_EVENTLISTENER
}  // namespace jht::os::event