#include "jht_os_window.hpp"
#include "impl/jht_os_impl_sdl.hpp"
#include <SDL2/SDL_vulkan.h>

namespace jht::os {

    Window::Window(const CreateInfo& ci)
    {
        _name                = ci.Name;
        const char* namecstr = reinterpret_cast<const char*>(_name.data());
        int32_t     x        = ci.X;
        int32_t     y        = ci.Y;
        if(ci.Auto)
        {
            if(ci.Centered)
            {
                x = y = SDL_WINDOWPOS_CENTERED;
            }
            else
            {
                x = y = SDL_WINDOWPOS_UNDEFINED;
            }
        }
        _windowHandle = SDL_CreateWindow(namecstr, x, y, ci.W, ci.H, ci.Flags);
        detail::AssertSDL(!!_windowHandle);
    }

    Window::~Window()
    {
        if(!_windowHandle)
        {
            return;
        }
        SDL_Window* handle = _windowHandle;
        SDL_DestroyWindow(handle);
        _windowHandle = nullptr;
    }

    Window& Window::SetTitle(std::u8string_view name)
    {
        _name                = name;
        const char* namecstr = reinterpret_cast<const char*>(_name.data());
        if(Exists())
        {
            SDL_SetWindowTitle(_windowHandle, namecstr);
        }
        return *this;
    }

    Window& Window::SetSize(int32_t w, int32_t h)
    {
        if(Exists())
        {
            SDL_SetWindowSize(_windowHandle, w, h);
        }
        return *this;
    }
    void Window::GetSize(int32_t& w, int32_t& h) const
    {
        if(Exists())
        {
            SDL_GetWindowSize(_windowHandle, &w, &h);
        }
    }
    Window& Window::SetPositionAutoUndefined()
    {
        if(Exists())
        {
            SDL_SetWindowPosition(_windowHandle, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED);
        }
        return *this;
    }
    Window& Window::SetPositionAutoCentered()
    {
        if(Exists())
        {
            SDL_SetWindowPosition(_windowHandle, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);
        }
        return *this;
    }
    Window& Window::SetPosition(int32_t x, int32_t y)
    {
        if(Exists())
        {
            SDL_SetWindowPosition(_windowHandle, x, y);
        }
        return *this;
    }
    void Window::GetPosition(int32_t& x, int32_t& y) const
    {
        if(Exists())
        {
            SDL_GetWindowPosition(_windowHandle, &x, &y);
        }
    }

    Window& Window::Hide()
    {
        if(Exists())
        {
            SDL_HideWindow(_windowHandle);
        }
        return *this;
    }
    Window& Window::Maximize()
    {
        if(Exists())
        {
            SDL_MaximizeWindow(_windowHandle);
        }
        return *this;
    }
    Window& Window::Minimize()
    {
        if(Exists())
        {
            SDL_MinimizeWindow(_windowHandle);
        }
        return *this;
    }
    Window& Window::BringToFront()
    {
        if(Exists())
        {
            SDL_RaiseWindow(_windowHandle);
        }
        return *this;
    }
    Window& Window::Restore()
    {
        if(Exists())
        {
            SDL_RestoreWindow(_windowHandle);
        }
        return *this;
    }
    Window& Window::SetFlash(EFlash flash)
    {
        if(Exists())
        {
            SDL_FlashOperation flashOp;
            switch(flash)
            {
                case EFlash::Briefly:
                    flashOp = SDL_FlashOperation::SDL_FLASH_BRIEFLY;
                    break;
                case EFlash::UntilFocus:
                    break;
                    flashOp = SDL_FlashOperation::SDL_FLASH_UNTIL_FOCUSED;
                case EFlash::Cancel:
                default:
                    flashOp = SDL_FlashOperation::SDL_FLASH_CANCEL;
                    break;
            }
            detail::AssertSDL(SDL_FlashWindow(_windowHandle, flashOp));
        }
        return *this;
    }
}  // namespace jht::os
