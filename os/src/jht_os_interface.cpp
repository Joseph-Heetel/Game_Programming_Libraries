#include "jht_os_interface.hpp"
#include "impl/jht_os_impl_sdl.hpp"
#include "jht_os_eventmanager.hpp"
#include <jht_ecs_serviceprovider.hpp>

namespace jht::os {
    OsInterface::OsInterface(ecs::ServiceProvider* serviceProvider) : ServiceDependent(serviceProvider), _windows(), _eventManager(nullptr), Windows(_windows)
    {
        detail::AssertSDL(SDL_Init(SDL_INIT_EVERYTHING));
    }

    Window* OsInterface::CreateWindow(const Window::CreateInfo& ci)
    {
        Window* result = new Window(ci);
        _windows.emplace(result);
        return result;
    }

    void OsInterface::DestroyWindow(Window*& window)
    {
        auto iter = _windows.find(window);
        if(iter != _windows.end())
        {
            _windows.erase(iter);
        }
        delete(Window*)window;
        window = nullptr;
    }

    inline void lGetEventManagerIfNeeded(ecs::ServiceProvider* serviceProvider, EventManager*& eventManager) 
    {
        if (!eventManager)
        {
            eventManager = serviceProvider->GetRequired<EventManager>();
        }
    }

    bool OsInterface::PollAndDistributeEvent()
    {
        lGetEventManagerIfNeeded(_serviceProvider, _eventManager);
        return _eventManager->PollAndDistribute();
    }

    OsInterface::~OsInterface()
    {
        SDL_Quit();
    }
}  // namespace jht::os
