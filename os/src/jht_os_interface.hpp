#pragma once
#include "jht_os_window.hpp"
#include "jht_readonlycollection.hpp"
#include <jht_ecs_service.hpp>
#include <jht_ecs_servicedependent.hpp>
#include <set>

namespace jht::os {
    class OsInterface : public ecs::IService, public ecs::ServiceDependent
    {
      public:
        inline virtual std::u8string_view GetId() const { return u8"jht::os::OsInterface"; }

        OsInterface(ecs::ServiceProvider* serviceProvider);

        Window* CreateWindow(const Window::CreateInfo& ci);
        void    DestroyWindow(Window*& window);

        bool PollAndDistributeEvent();

        virtual ~OsInterface();

      protected:
        std::set<Window*> _windows;
        EventManager* _eventManager;

      public:
        readonlycollection<std::set<Window*>> Windows;
    };

}  // namespace jht::os
