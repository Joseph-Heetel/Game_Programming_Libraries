#include "jht_os_eventmanager.hpp"
#include "jht_os_event.hpp"
#include <SDL2/SDL.h>
#include <jht_ecs_callbackhub.hpp>
#include <jht_ecs_serviceprovider.hpp>
#include <jht_ecs_simplecallbacklist.hpp>
#include <limits>

namespace jht::os {
    EventManager::EventManager(ecs::ServiceProvider* serviceProvider) : ServiceDependent(serviceProvider), _callbackHub(nullptr) {}

    inline void lGetCallbackhubIfNeeded(ecs::ServiceProvider* serviceProvider, ecs::CallbackHub*& callbackHub)
    {
        if(!callbackHub)
        {
            callbackHub = serviceProvider->GetRequired<ecs::CallbackHub>();
        }
    }

    inline event::InputAxis* lTranslateSdlEventJoyAxis(const SDL_Event& event)
    {
        uint64_t timestamp = event.jaxis.timestamp;
        fp64_t   state     = static_cast<fp64_t>(event.jaxis.value) / std::numeric_limits<int16_t>::max();
        // TODO: get correct input device and input component
        return new event::InputAxis(event.jaxis.timestamp, nullptr, nullptr, state);
    }

    inline event::InputButton* lTranslateSdlEventKey(const SDL_Event& event)
    {
        uint64_t timestamp = event.key.timestamp;
        bool     state     = event.key.state == SDL_PRESSED;
        // TODO: get correct input device and input component
        return new event::InputButton(event.jaxis.timestamp, nullptr, nullptr, state);
    }

    inline event::InputButton* lTranslateSdlEventJoyKey(const SDL_Event& event)
    {
        uint64_t timestamp = event.jbutton.timestamp;
        bool     state     = event.jbutton.state == SDL_PRESSED;
        // TODO: get correct input device and input component
        return new event::InputButton(event.jaxis.timestamp, nullptr, nullptr, state);
    }

    bool EventManager::PollAndDistribute()
    {
        lGetCallbackhubIfNeeded(_serviceProvider, _callbackHub);
        SDL_Event event;
        if(SDL_PollEvent(&event) == 1)
        {
            event::Base* arg = nullptr;
            switch(event.type)
            {
                case SDL_DROPTEXT:  // TODO Low Priority Handle Droptext/file events
                case SDL_DROPFILE:  // WindowPtr Drop Event
                    break;
                case SDL_KEYUP:
                case SDL_KEYDOWN: {
                    // Binary Event
                    arg = lTranslateSdlEventKey(event);
                    _callbackHub->Invoke<event::InputButtonListener>((event::InputButton*)arg);
                    break;
                }
                case SDL_EventType::SDL_JOYAXISMOTION: {
                    arg = lTranslateSdlEventJoyAxis(event);
                    _callbackHub->Invoke<event::InputAxisListener>((event::InputAxis*)arg);
                    break;
                }
                case SDL_JOYBALLMOTION:
                case SDL_JOYHATMOTION:
                    // TODO Low Priority: Handle Joystick Ball & Hat input as directional
                    break;
                case SDL_JOYBUTTONUP:
                case SDL_JOYBUTTONDOWN: {
                    arg = lTranslateSdlEventJoyKey(event);
                    _callbackHub->Invoke<event::InputButtonListener>((event::InputButton*)arg);
                    break;
                }
                case SDL_JOYDEVICEADDED:
                case SDL_JOYDEVICEREMOVED:
                    break;
                case SDL_MOUSEMOTION:
                    // Mouse Move Event
                    break;
                case SDL_MOUSEBUTTONUP:
                case SDL_MOUSEBUTTONDOWN:
                    // Binary Event
                    break;
                case SDL_MOUSEWHEEL:
                    break;
                case SDL_WINDOWEVENT:
                    // Multiple WindowPtr Events
                    {
                        SDL_WindowEvent wevent = event.window;
                        switch(wevent.event)
                        {
                            case SDL_WINDOWEVENT_CLOSE:
                                break;
                            case SDL_WINDOWEVENT_SIZE_CHANGED:
                                break;
                            case SDL_WINDOWEVENT_RESTORED:
                                break;
                            case SDL_WINDOWEVENT_ENTER:
                                break;
                            case SDL_WINDOWEVENT_LEAVE:
                                break;
                            case SDL_WINDOWEVENT_FOCUS_GAINED:
                                break;
                            case SDL_WINDOWEVENT_FOCUS_LOST:
                                break;
                            case SDL_WINDOWEVENT_MINIMIZED:
                            case SDL_WINDOWEVENT_MAXIMIZED:
                            case SDL_WINDOWEVENT_RESIZED:  // Ignored since size_changed is called either way
                            case SDL_WINDOWEVENT_SHOWN:
                            case SDL_WINDOWEVENT_HIDDEN:
                            case SDL_WINDOWEVENT_EXPOSED:
                            case SDL_WINDOWEVENT_MOVED:
                            case SDL_WINDOWEVENT_TAKE_FOCUS:
                            case SDL_WINDOWEVENT_HIT_TEST:
                                break;
                        }
                        break;
                    }
                case SDL_CONTROLLERAXISMOTION:  // the SDL controller subsystem is not initialized, controllers are treated like any joystick
                case SDL_CONTROLLERBUTTONUP:
                case SDL_CONTROLLERBUTTONDOWN:
                case SDL_QUIT:  // Quit Event (Handled explicitly via window events at a higher level)
                default:
                    break;
            }
            if(!!arg)
            {
                _callbackHub->Invoke<event::BaseListener>(arg);
                delete arg;
            }
        }
        return SDL_PollEvent(nullptr) == 1;
    }
    std::vector<ecs::ICallbackListBase*> EventManager::GetCallbackLists() const
    {
        return {new ecs::SimpleCallbackList<event::BaseListener>(),
                new ecs::SimpleCallbackList<event::InputButtonListener>(),
                new ecs::SimpleCallbackList<event::InputAxisListener>(),
                new ecs::SimpleCallbackList<event::InputDirectionalListener>(),
                new ecs::SimpleCallbackList<event::InputDevicesChangedListener>(),
                new ecs::SimpleCallbackList<event::WindowResizedListener>(),
                new ecs::SimpleCallbackList<event::WindowFocusListener>(),
                new ecs::SimpleCallbackList<event::WindowCloseRequestListener>()};
    }

}  // namespace jht::os
