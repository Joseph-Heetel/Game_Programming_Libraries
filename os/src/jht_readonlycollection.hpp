#pragma once
#include <vector>

namespace jht {
    template <typename TCOLLECTION_>
    class readonlycollection
    {
        using COL   = TCOLLECTION_;
        using ITER  = typename TCOLLECTION_::iterator;
        using CITER = typename TCOLLECTION_::const_iterator;

      public:
        inline constexpr readonlycollection(COL& col) : _col(col) {}
        inline constexpr readonlycollection(const readonlycollection& other) : _col(other._col) {}
        inline constexpr readonlycollection(const readonlycollection&& other) : _col(other._col) {}
        inline constexpr readonlycollection& operator=(const readonlycollection& other) { _col = other._col; }
        inline constexpr ~readonlycollection() = default;

        inline constexpr ITER begin() noexcept { return _col.begin(); }

        inline constexpr ITER end() noexcept { return _col.end(); }

        inline constexpr CITER cbegin() noexcept { return _col.cbegin(); }

        inline constexpr CITER cend() noexcept { return _col.cend(); }

      protected:
        COL& _col;
    };
}  // namespace jht
