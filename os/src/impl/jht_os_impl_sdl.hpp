#pragma once
#include <SDL2/SDL.h>
#include <stdexcept>


namespace jht {
    namespace os {
        namespace detail {

            inline void AssertSDL(bool v)
            {
                if(!(v))
                {
                    const char* error = SDL_GetError();
                    throw std::runtime_error(error);
                }
            }
            inline void AssertSDL(int ret)
            {
                if(ret != 0)
                {
                    const char* error = SDL_GetError();
                    throw std::runtime_error(error);
                }
            }

        }  // namespace detail
    }      // namespace os
}  // namespace jht
