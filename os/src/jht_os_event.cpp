#include "jht_os_event.hpp"

namespace jht::os::event {

    namespace detail {
        WindowBase::WindowBase(Window* window) : SourceWindow(window) {}
        InputBase::InputBase(InputDevice* inputDevice) : SourceInputDevice(inputDevice) {}
        ButtonBase::ButtonBase(ButtonInputComponent* inputComponent, bool pressed) : SourceInputComponent(inputComponent), Pressed(pressed) {}
        AxisBase::AxisBase(AxisInputComponent* inputComponent, fp64_t state) : SourceInputComponent(inputComponent), State(state) {}
        DirectionalBase::DirectionalBase(DirectionalInputComponent* inputComponent, int32_t deltaX, int32_t deltaY)
            : SourceInputComponent(inputComponent), DeltaX(deltaX), DeltaY(deltaY)
        {
        }
        MouseMovedBase::MouseMovedBase(int32_t posX, int32_t posY, int32_t deltaX, int32_t deltaY) : PosX(posX), PosY(posY), DeltaX(deltaX), DeltaY(deltaY) {}
        DevicesChangedBase::DevicesChangedBase(InputDevice* added, InputDevice* removed) : Added(added), Removed(removed) {}
        ResizedBase::ResizedBase(uint32_t width, uint32_t height) : Width(width), Height(height) {}
        FocusBase::FocusBase(bool mouseFocus, bool inputFocus) : MouseFocus(mouseFocus), InputFocus(inputFocus) {}
    }  // namespace detail


    InputButton::InputButton(uint64_t timestamp, InputDevice* device, ButtonInputComponent* component, bool pressed)
        : Base(timestamp), detail::InputBase(device), detail::ButtonBase(component, pressed)
    {
    }

    InputAxis::InputAxis(uint64_t timestamp, InputDevice* device, AxisInputComponent* component, fp64_t state)
        : Base(timestamp), detail::InputBase(device), detail::AxisBase(component, state)
    {
    }

    InputDirectional::InputDirectional(uint64_t timestamp, InputDevice* device, DirectionalInputComponent* component, int32_t deltaX, int32_t deltaY)
        : Base(timestamp), detail::InputBase(device), detail::DirectionalBase(component, deltaX, deltaY)
    {
    }

    InputDevicesChanged::InputDevicesChanged(uint64_t timestamp, InputDevice* added, InputDevice* removed) : Base(timestamp), detail::DevicesChangedBase(added, removed) {}

    WindowResized::WindowResized(uint64_t timestamp, Window* window, uint32_t width, uint32_t height)
        : Base(timestamp), detail::WindowBase(window), detail::ResizedBase(width, height)
    {
    }

    WindowFocus::WindowFocus(uint64_t timestamp, Window* window, bool mouseFocus, bool inputFocus)
        : Base(timestamp), detail::WindowBase(window), detail::FocusBase(mouseFocus, inputFocus)
    {
    }

    WindowCloseRequest::WindowCloseRequest(uint64_t timestamp, Window* window) : Base(timestamp), detail::WindowBase(window) {}
}  // namespace jht::os::event